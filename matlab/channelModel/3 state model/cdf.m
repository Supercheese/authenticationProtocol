function [b,y]=cdf(z)
[a,b]=hist(z,15);
a=a/length(z);
 y= cumsum(a);
