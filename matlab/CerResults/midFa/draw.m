%% plot di Pbit(epsiolon) al ricevitore di Eve per vari R2

clear all
close all

load CerRectM8R2u1p5385
figure;
hold on
p=semilogy(delay(Pbit>0),Pbit(Pbit>0));
p.DisplayName=['R2=',num2str(R2)];
p.LineWidth=1.5;

% load CerRectM8R2u1p5789
% p=semilogy(delay(Pbit>0),Pbit(Pbit>0));
% p.DisplayName=['R2=',num2str(R2)];

load CerRectM8R2u1p7647
p=semilogy(delay(Pbit>0),Pbit(Pbit>0));
p.DisplayName=['R2=',num2str(R2)];
p.LineWidth=1.5;

load CerRectM8R2u2
p=semilogy(delay(Pbit>0),Pbit(Pbit>0));
p.DisplayName=['R2=',num2str(R2)];
p.LineWidth=1.5;

grid on
legend('show')
xlim([0.1,0.5]);
xlabel('$\epsilon/T_c$','Interpreter','latex')
ylabel('$P_{bit}$','Interpreter','latex')

title('Rect $R1=3$','Interpreter','latex')

figure
hold on

load CerTwoHornM8R2u1p5075
p=semilogy(delay(Pbit>0),Pbit(Pbit>0));
p.DisplayName=['R2=',num2str(R2)];
p.LineWidth=1.5;

load CerTwoHornM8R2u1p5385
p=semilogy(delay(Pbit>0),Pbit(Pbit>0));
p.DisplayName=['R2=',num2str(R2)];
p.LineWidth=1.5;

load CerTwoHornM8R2u1p5789_2
PbitFin=Pbit;
delayFin=delay;

load CerTwoHornM8R2u1p5789_3
PbitFin=[PbitFin Pbit];
delayFin=[delayFin delay];

p=semilogy(delayFin(PbitFin>0),PbitFin(PbitFin>0));
p.DisplayName=['R2=',num2str(R2)];
p.LineWidth=1.5;

load CerTwoHornM8R2u1p7647
p=semilogy(delay(Pbit>0),Pbit(Pbit>0));
p.DisplayName=['R2=',num2str(R2)];
p.LineWidth=1.5;

% load CerTwoHornM8R2u1p5
% p=semilogy(delay(Pbit>0),Pbit(Pbit>0));
% p.DisplayName=['R2=',num2str(R2)];
% p.LineWidth=1.5;

grid on
legend('show')
% xlim([0.1,0.5]);
xlabel('$\epsilon/T_c$','Interpreter','latex')
ylabel('$P_{bit}$','Interpreter','latex')
title('Two Horn $R1=3$','Interpreter','latex')
xlim([0.1,0.18])