function [ Pe ] = normalApproxF(log2M,n, Gamma )

R=log2M/n;
C=.5*log2(1+Gamma);
V=Gamma*(2+Gamma)/(2*(1+Gamma)^2);
Pe=qfunc(sqrt(n/V)*( (C-R)/log2(exp(1)) + log(n)/(2*n)));

end

