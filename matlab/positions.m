clear all

er=6371*1e3; % Earth radius
c=299792458; % speed of light
slength=4e-3; % symbol length
clength=slength/4092; %chip length
teve=clength/2;

%satellite coordinates in km
ys1=29593; %
xs=1000;
ys2=sqrt(ys1^2-xs^2);
zs=1000;
ys3=ys2;
s1=[0 ys1 0];
s2=[xs ys2 0];
s3=[0 ys3 zs];
s1=s1*1e3; s2=s2*1e3; s3=s3*1e3; % [m]

xutrue=10*1e3; %10 km
zutrue=10*1e3; %10 km
yutrue=sqrt(er^2-xutrue^2-zutrue^2);
utrue=[xutrue yutrue zutrue];

rhoHat1=sqrt(s1*utrue');
rhoHat2=sqrt(s2*utrue');
rhoHat3=sqrt(s3*utrue');

ttrue1=rhoHat1/c;
ttrue2=rhoHat2/c;
ttrue3=rhoHat3/c;

rho1=c*(ttrue1+teve);
rho2=c*(ttrue2+teve);
rho3=c*(ttrue3+teve);

rhat1=sqrt(sum((s1-utrue).^2));
rhat2=sqrt(sum((s2-utrue).^2));
rhat3=sqrt(sum((s3-utrue).^2));

a1=(s1-utrue)/rhoHat1;
a2=(s2-utrue)/rhoHat1;
a3=(s3-utrue)/rhoHat1;
A=[a1; a2; a3];

Drho1=rhoHat1-rho1;
Drho2=rhoHat2-rho1;
Drho3=rhoHat3-rho1;
Drho=[Drho1; Drho2; Drho3];

Du=inv(A)*Drho