clc;
clear all;
close all;
%% simulation  parameters 
% uu_satcomsys_simu_parameters;
simu_parameters;
%% loo series  parameters 
% uu_satcomsys_loo_series_parameters;
loo_series_parameters;
%% 
% [StateSeries]=uu_satcomsys_MarkovStateSeries;
[StateSeries]=MarkovStateSeries;
d_axisInterp=[0:length(StateSeries)-1]*sample_space;
subplot(211)
plot(d_axisInterp,StateSeries,'r')
aa=axis;
axis([aa(1) aa(2)  0 4])
xlabel('Travelled time(seconds)')
title('StateSeries')
%% loo series for each state 
[loo_state1] = loo_series(mean_state1,sigma_state1,multipath_power_state1);
[loo_state2] = loo_series(mean_state2,sigma_state2,multipath_power_state2);
[loo_state3] = loo_series(mean_state3,sigma_state3,multipath_power_state3);
time_series=zeros(1,min(length(abs(loo_state1)),length(StateSeries)));
%%   -------------------------Calculation of time series---------------------------------
for i=1:min(length(abs(loo_state1)),length(StateSeries))
    if (StateSeries(i)==1)
        time_series(i)=loo_state1(i);
    elseif (StateSeries(i)==2)
        time_series(i)=loo_state2(i);
    elseif (StateSeries(i)==3)
         time_series(i)=loo_state3(i);
    end
end
%% --------------------------Magnitude of channel complex envelope-----------------------
 Magnitude_complex_envelope=20*log10(abs(time_series))-max(20*log10(abs(time_series)));
 subplot(212)
 plot(d_axisInterp(1:length(Magnitude_complex_envelope)),Magnitude_complex_envelope);
 xlabel('travelled time');
 ylabel(' Normalized level in dB');
 title('Magnitude of complex Envelope');
%% --------------------------cummulative distribution function---------------------------
  [x,y]=cdf(abs(time_series));
  figure, plot(20*log10(x)-max(20*log10(x)),y);
  xlabel('Normalized level in dB');
 ylabel('probability the abscissa is not exceeded')
  title('cdf');
 
 %%  ----------------------- probability density function------------------------------------
  [b,a1]=pdf(abs(time_series));
  figure,plot(20*log10(abs(b))-max(20*log10(abs(b))),a1);
  xlabel('Normalized Level in dB');
  ylabel('pdf')
  title('pdf of Magnitude of Complex Envelope');
  
 
%% --------------------------level crossing rate--------------------------------------------
 rmsr=sqrt(std(abs(time_series))^2+mean(abs(time_series))^2);      % calculate rms value of signal magnitide
 rrho=abs(time_series)/rmsr;  
 [axislcr,lcr]=lcr(abs(rrho),1/fs);
 lcr=lcr/max_doppler_shift;
 figure,semilogy(axislcr,lcr)
 xlabel('Normalized Level in dB')
 ylabel('crossings/wavelength')
 title('level crossing rate') 
 
 
%% --------------------------------Average fade duration---------------------------
  [axisafd,afd]=afd(rrho,1/fs);
  figure,semilogy(axisafd,afd/max_doppler_shift);
  xlabel('Normalized Level in dB')
  title('average fade duration')
   
%% --------------------------------- scatterplot---------------------------------------
 konst_qpsk = exp(1i*[pi/4 3*pi/4 5*pi/4 7*pi/4]).'; % QPSK alphabet
 qpsk = konst_qpsk(randi(4,length(time_series),1))'; % QPSK symbol sequence
 scatterplot(qpsk);
 rx_sig=qpsk.*time_series;
 scatterplot(rx_sig)


%% -----------------------------auto-correlation----------------------------------------
 N=length(Magnitude_complex_envelope);
 lg=0.01*N;
 for m=1:lg
     for n=1:N+1-m
         xs(m,n)=Magnitude_complex_envelope(n-1+m);
     end
 end
 r1=(xs)*(Magnitude_complex_envelope');
 r=r1'./N;
 lag=1:m;
 figure,plot(lag,r/max(r))
 xlabel('Lag')
 ylabel('Autocorrelation')


%% --------------------------- Doppler Spectrum------------------------------------------
psd1=abs(fftshift(fft(time_series,NFFT)/NFFT)).^2;
freqaxis=(-(NFFT/2):(NFFT/2)-1)*2*max_doppler_shift/NFFT;
figure,plot(freqaxis,psd1)
xlabel('Doppler Shift');
ylabel('Doppler Spectrum');
figure,plot(freqaxis,10*log10(psd1)-max(10*log10(psd1)))
xlabel('Doppler Shift');
ylabel('Normalized Doppler Spectrum');


% ------------------------ Correlation coefficient --------------------------------------
correl=xcorr(abs(time_series)-mean(abs(time_series)),'coeff');
correlaxis=([0:length(correl)-1]-length(correl)/2+1)*(lambdac/sampling_factor);
figure,plot(correlaxis,correl);
axis([0 1 -0.35 1])
xlabel('Spacing (m)')
ylabel('Complex envelope autocorrelation coefficient')
title('correlation coefficient')

% -------------------------phase variations of channel complex envelope-------------------
subplot(211)
plot(d_axisInterp,StateSeries,'r')
aa=axis;
axis([aa(1) aa(2)  0 4])
xlabel('Travelled time(seconds)')
title('StateSeries')
phase=angle(time_series);
phase_abs=unwrap(phase);
subplot(212)
plot(d_axisInterp(1:length(phase_abs)),phase_abs)
xlabel('travelled distance')
ylabel('phase')
[a,b]=hist(phase,30);
bin=b(2)-b(1);
a=a/length(phase)/bin;
figure,plot(b,a);
title('pdf of Complex Envelope phase')
