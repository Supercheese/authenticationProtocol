clear all
close all

syms Gamma(e,A1,A2,B1,x,B,A)
Gamma = ((A1 + A2*e)^2 *x) / ((B1*e)^2 * x + B + 2*(A-x)*(1-A1-A2*e));
Gammap = diff(Gamma,e);
Gammapp = diff(Gammap,x);
CB = 0.5 * log2(1+Gamma);

load rangingCodes.mat
T_c = 1;
c1 = c1 /sqrt(length(c1)*T_c);
mixedSum = c1(2:end)*c1(1:end-1)';
squareSum = sum(c1.^2);

A1 = squareSum*T_c;
A2 = (mixedSum - squareSum);
alpha = A1+A2*T_c/4;
B1 = c1(1)*c1(end);
e = T_c/10;
beta = B1 * e;
figure(1)
figure(2)
for B = db2pow([-10,-5,0])
    Aval = db2pow(-10:0.5:10);
    GammapPlot = zeros(1,length(Aval));
    optimum = zeros(1,length(Aval));
    for k =2:length(Aval)
        A = Aval(k);
        syms x
        [n2,d2] = numden(subs(Gammapp));
        p = coeffs(n2);
        candidate = double(roots(fliplr(p)));
        
%         figure(3);
%         fplot(subs(Gammap),[0,10])
%         figure(4)
%         fplot(subs(Gammapp),[0,10])
%         close 3
%         close 4
        Rx = 0.5;
        num = (1-2^(2*Rx)) * (B + 2*A - 2*A*alpha);
        den = (1-2^(2*Rx)) * (beta^2 - 2 + 2*alpha) - alpha^2;
        bound = min(num / den,A);       
        
        candidateValue = 0;
        if candidate < bound
            x = candidate;
            candidateValue = double(subs(Gammap));
            syms x
        end
        
        x=0;
        val0 = double(subs(Gammap));
        syms x
        
        x = bound;
        valBound = double(subs(Gammap));
        syms x
        
        solVec = [candidateValue val0 valBound];
        points = [candidate, 0, bound];
        [solValue,pointsIdx] = min(solVec);
        optimum(k) = points(pointsIdx(1));
        x = points(pointsIdx(1));
        GammapPlot(k) = double(subs(Gammap));
    end
    
    figure(1)
    p = plot(pow2db(Aval),GammapPlot);
    p.LineWidth = 1.5;
    p.DisplayName =  "$\sigma^2_{w_B} = " + num2str(pow2db(B) + "$") + " dB";
    hold on
    xlim([-10, 4.7])
    
    figure(2)
    p = plot(pow2db(Aval),optimum);
    p.LineWidth = 1.5;
    p.DisplayName =  "$\sigma^2_{w_B} = $" + num2str(pow2db(B)) + " dB";
    hold on
end
figure(1)
ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui('center')
ax.FontSize=10;
grid on
xlabel("$A$ [dB]",'Interpreter','latex')
ylabel("$f(\sigma^2_{opt})$",'Interpreter','latex')
l=legend('show');
l.Interpreter='latex';
l.FontSize=12;
l.Location='southwest';


figure(2)
p=plot(pow2db(Aval),Aval,'--k');
p.DisplayName = "$A$";
ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui('center')
ax.FontSize=10;
grid on
xlabel("$A$ [dB]",'Interpreter','latex')
ylabel("$\sigma^2_{opt}$",'Interpreter','latex')
l=legend('show');
l.Interpreter='latex';
l.FontSize=12;
l.Location='northwest';
xlim([-10, 10])