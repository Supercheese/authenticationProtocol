function out=integrand(re,im,sigmaw2,M)



omega=0:M-1;
a=exp(1j*2*pi*omega/M);
rea=real(a);
ima=imag(a);
out=0;
for z=1:M
    abs2=(re-rea(z)).^2+(im-ima(z)).^2;
    out=out+(1/(2*pi*sigmaw2))*exp(-abs2/(2*sigmaw2))*(1/M);
end
out=real(out);
out=out.*log2(1./out);
end