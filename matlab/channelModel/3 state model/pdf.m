function [b,k]=pdf(r)
[a,b]=hist(r,30);
  bin=b(2)-b(1);
k=a/length(r)/bin;
end