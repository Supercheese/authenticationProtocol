function [ out ] = triang(a, T, tau )
    
    out=a^2*(T-tau);
    out(tau>T)=0;


end

