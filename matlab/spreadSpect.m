clear all;
%close all

scalProd= @(x,y) sum(x.*y); 

plots=0;

ndata=100; %number of symbol data
nSamp=8;% samples per pulse (continuos time approx)
Ns=2^8; % length of spreading seq
tt=nSamp*Ns; %total taps for one symbol
Nit=100;

% 2 spreading seq
A=hadamard(Ns);
ss1=A(2,:);
ss2=A(4,:);

% SNR for w_B : Pp/sigmawB2
SNRwB_dB=-20;
SNRwB=db2pow(SNRwB_dB);

%delay in number of taps
delay=0:-1:-20;

% orthonormal basis
base1=[ones(1,nSamp) zeros(1,(Ns-1)*nSamp)];
PHI=zeros(Ns,tt);
PHI(1,:)=base1;
for z=2:Ns
    PHI(z,:)=circshift(PHI(z-1,:),nSamp);
end
Er=sum(PHI(1,:).*PHI(1,:)); %energy of rect pulse
PHI=sqrt((1/Er))*PHI;

dnerr=zeros(1,length(SNRwB));
xnerr=zeros(1,length(SNRwB));
xdnerr=zeros(1,length(delay));
for it=1:Nit
    
    % navigation data
    d=sign(randn(1,ndata));
    
    % p(t)
    p=rectpulse(ss1,nSamp)/sqrt(Er); %normalize rect pulse
    Pp=1/nSamp; %power of p(t)
    p=kron(d,p); % multiply by data
    
    % x(t)
    v=sign(randn(1,ndata));
    rect=0;
    if rect
        x=rectpulse(ss2,nSamp)/sqrt(Er); %normalize. Unit energy pulse
    else
        x=buildPulse(nSamp,2,ss2);
    end
    x=kron(v,x);
    
    for m=1:length(SNRwB)
        sigmawB2=Pp/SNRwB(m);
        
        % w_B
        wB=sqrt(sigmawB2)*randn(1,tt*ndata);
        
        % noise wStar
        wStar=[];
        for z=1:ndata
            temp=randn(1,tt); %build noise sample
            cw=PHI*temp'; % project
            temp=temp-sum(cw.*PHI); % subtract
            wStar=cat(2,wStar,temp); % concatenate
        end
        wStar=2*wStar;
        
        
        sOrt=x+wStar+wB; %prepare orthogonal stuff
        for dl=1:length(delay) % if delay < 0 --> actual delay. Anticipation otherwise. 0 if nothing
            s=p+circshift(sOrt,delay(dl));
            
            %receiver
            d_hat=zeros(1,ndata);
            v_hat=zeros(1,ndata);
            s=s-wStar; %subtract wStar
            for z=1:ndata
                r=(PHI*s((z-1)*tt+1:z*tt).');
                d_hat(z)=sign(scalProd(r,ss1')/Ns);
                v_hat(z)=sign(scalProd(r,ss2')/Ns);
            end
            
            dnerr(m)= dnerr(m)+sum(d_hat~=d);
            xnerr(m)= xnerr(m)+sum(v_hat~=v);
            xdnerr(dl)=xdnerr(dl) + sum(v_hat~=v);
        end
    end
end

Ped=dnerr/(ndata*Nit);
Pex=xnerr/(ndata*Nit);
Pexd=xdnerr/(ndata*Nit);
accuracy=10/(ndata*Nit);

if plots
    figure;
    semilogy(SNRwB_dB,Ped)
    title("Pe on data for SNR_{wB}")
end

figure;
semilogy(delay,Pexd)
grid on
xlabel("delay taps")
ylabel("P_e")