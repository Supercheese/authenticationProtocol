%% Compute how Cs degrades with delay. Load SNR delayed to get equivalent sigmawB. Complex PSK channel

clear all
close all
load SNRbob1hornStandardDelAnt
warning('error','MATLAB:integral2:nonFiniteResult');
sigmawB=1./SNRBob;
sigmawBhalf=sigmawB/2; %from loaded data
sigmawstar=db2pow(-6);
sigmawstarhalf=sigmawstar/2;

pos=sigmawBhalf<sigmawstarhalf;
sigmawBsim=sigmawB(pos);
sigmawBhalfsim=sigmawBhalf(pos);
Cs=zeros(size(sigmawB));

figure;
hold on
for M=[2 4 8 16]% 32 64]
    
    fun=@(x,y) integrand(x,y,sigmawstarhalf,M);
    CE=integral2(fun,-3,3,-3,3)-log2(2*pi*exp(1)*sigmawstarhalf);
    % fsurf(fun)
    
    CB=zeros(size(sigmawBsim));
    xyInterval=50*[-1,1,-1,1];
    IO=[1 -1 1 -1];
    for z=1:length(sigmawBsim)
        fun=@(x,y) integrand(x,y,sigmawBhalfsim(z),M);
        %     fsurf(fun,xyInterval)
        II=[-3 3 -3 3];
        success=0;
        while ~success
            try
                CB(z)=integral2(fun,II(1),II(2),II(3),II(4))-log2(2*pi*exp(1)*sigmawBhalfsim(z));
                success=1;
            catch w
                II=II+IO;
            end
        end        
    end
    Cs(pos)=CB-CE;
    p=plot(delay,Cs);
    p.LineWidth=1.5;
    p.DisplayName=['M = ' num2str(M)];
end

% Gaussian case
CsG=log2(1+SNRBob)-log2(1+db2pow(6));
CsG(CsG<0)=0;
p=plot(delay,CsG,'k');
p.LineWidth=1.5;
p.DisplayName='Gaussian';

l=legend('show');
l.FontSize=14;
grid on
xlabel('$\epsilon/T_c$','Interpreter','latex','FontSize',14)
ylabel('$C_s \ [bit/s/Hz]$','Interpreter','latex','FontSize',14)
ax=gca;
gca.FontSize=13.5;

warning('on','MATLAB:integral2:nonFiniteResult');