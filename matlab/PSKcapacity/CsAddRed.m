clear all
close all
load CBsigmawstar
load CEsigmawstar
fig1=0;
fig2=0;
fig3=0;
fig31=0;
fig4=0;
fig5=1;
fig6=fig5;

%% Ceq vs CE fig 1
if fig1
    rhoval=[1 3/4 1/2 1/4 1/8 0];
    figure
    hold on
    for rho=rhoval
        Ceq=(1-rho)*CB/4+rho;
        p=plot(sigmawstar,Ceq);
        p.LineWidth=1.5;
        p.DisplayName=['rho=',num2str(rho)];
        if rho==0
            p.DisplayName=['rho=',num2str(rho),', $C(w_B+w^*)$'];
        end
    end
    
    p=plot(sigmawstar,CE/4,'k--');
    p.LineWidth=1.5;
    p.DisplayName=['$C_E$'];
    grid on
    l=legend('show');
    l.Interpreter='latex';
    xlabel('$\sigma_{w^*}^2$ [dB]','Interpreter','latex','FontSize',14)
    title('$C_{eq}$ and $C_E$','Interpreter','Latex'...
        ,'FontSize',15)
end
%% fix Rs(rho) varying R1 fig 2
if fig2
    figure
    hold on
    swsf=-6;
    swsfIdx=find(sigmawstar==swsf);
    R1val=CE(swsfIdx):10;
    % rho=0:.1:.9;
    CA=10;
    for R1=R1val
        rhoMin=(R1-CB(swsfIdx))/(CA-CB(swsfIdx)+R1); % vincolo di decodificabilitÓ
        rho=rhoMin:.01:1;
        Cs=max(min((1-rho)*(R1-CE(swsfIdx)),R1-CA*rho./(1-rho)),0);
        p=plot(rho,Cs);
        p.LineWidth=1.5;
        p.DisplayName=['R1=',num2str(R1)];
    end
    grid on
    l=legend('show');
    l.Interpreter='latex';
    xlabel('$\rho$','Interpreter','latex','FontSize',14)
    ylabel('$R_s$','Interpreter','latex','FontSize',14)
end
%% fix R1 move rho. fig 3
if fig3
    figure
    hold on
    swsf=-6;
    swsfIdx=find(sigmawstar==swsf);
    R1=CE(swsfIdx):.1:10;
    % rho=0:.1:.9;
    CAval=[4 6 12 50 100];
    for CA=CAval
        rhoMin=(R1-CB(swsfIdx))./(CA-CB(swsfIdx)+R1)+.1;
        arg1=max((1-rhoMin).*(R1-CE(swsfIdx)),0);
        arg2=max(R1-CA.*rhoMin./(1-rhoMin),0);
        Cs=min(arg1,arg2);
%         p=plot(R1,arg1,R1,arg2);
%         p(1).Color=p(2).Color;
        p=plot(R1,Cs);
        p.LineWidth=1.5;
        p.DisplayName=['CA=',num2str(CA)];
    end
    grid on
    l=legend('show');
    l.Interpreter='latex';
    xlabel('$R_1$','Interpreter','latex','FontSize',14)
    ylabel('$R_s$','Interpreter','latex','FontSize',14)
    an=annotation('textarrow');
    an.X=[0.6,0.4];
    an.Y=[0.6,0.6];
    an.String='$+\ C_A \ - $';
    an.Interpreter='latex';
    an.VerticalAlignment='cap';
    an.HorizontalAlignment='center';
    title('16-PSK')
end
%% R1 and rhomin
if fig31
    figure
    hold on
    swsf=-6;
    swsfIdx=find(sigmawstar==swsf);
    R1=CE(swsfIdx):.1:10;
    % rho=0:.1:.9;
    CAval=[4 6 12 50 100];
    for CA=CAval
        rhoMin=(R1-CB(swsfIdx))./(CA-CB(swsfIdx)+R1);
        p=plot(R1,rhoMin);
%         p=plot(R1,arg1,R1,arg2);
%         p(1).Color=p(2).Color;
        p=plot(R1,Cs);
        p.LineWidth=1.5;
        p.DisplayName=['CA=',num2str(CA)];
    end
    grid on
    l=legend('show');
    l.Interpreter='latex';
    xlabel('$R_1$','Interpreter','latex','FontSize',14)
    ylabel('$R_s$','Interpreter','latex','FontSize',14)
    title('16-PSK')
end

%% Gaussian capacities. Fig 4
if fig4
    figure
    hold on
    sigmawB=-16;
    sigmawBLin=db2pow(sigmawB);
    sigmawstar=-6;
    sigmawstarLin=db2pow(sigmawstar);
    SNRBob=1/(sigmawstarLin+sigmawBLin);
    SNREve=-sigmawstar;
    SNREvelin=db2pow(SNREve);
    CE=log2(1+SNREvelin);
    CB=log2(1+SNRBob);
    R1=CE:.1:CE+10;
    CAval=4:6:16;
    for CA=CAval
        rhoMin=(R1-CB)./(CA-CB+R1);
        Cs=max(min((1-rhoMin).*(R1-CE),R1-CA.*rhoMin./(1-rhoMin)),0);
        p=plot(R1,Cs);
        p.LineWidth=1.5;
        p.DisplayName=['$C_A=$',num2str(CA)];
        check=rhoMin<R1./(R1-1);
    end
    grid on
    l=legend('show');
    l.Interpreter='latex';
    xlabel('$R1$','Interpreter','latex','FontSize',14)
    ylabel('$R_s$','Interpreter','latex','FontSize',14)
    title('Gaussian capacities')
end

%% removed noise, Gaussian,degradation Fig 5
if fig5
    load SNRbobRectDelAnt
    sigmawB=db2pow(-16);
    SNRB=1/(sigmawB);
    CB=log2(1+SNRBob);
    sigmawstar=db2pow(-6);
    SNRE=1/(sigmawstar);
    CE=log2(1+SNRE);
    Cs=CB-CE;
    figure 
    hold on
    plot(delay,max(Cs,0),'LineWidth',1.5,'DisplayName','noise removal')
    grid on
    xlabel('$\epsilon/T_c$','Interpreter','Latex')
    ylabel('$C_s$','Interpreter','Latex')
    
    
end

%% Gaussian capacities. Degradation Fig 6
if fig6

    load equivTapsRectDelay
    sigmawB=db2pow(-16);
    sigmawstar=db2pow(-6);
    SNRB=(alpha.^2)./(sigmawB+sigmawstar+beta.^2);
    CE=log2(1+1/sigmawstar);
    CB=log2(1+SNRB);
    
    CAval=4;
    for CA=CAval
        
        Cs=max(CB,0);
        p=plot((0:100)/100,Cs);
        p.LineWidth=1.5;
        p.DisplayName=['Additional redundancy'];
        
    end
    grid on
    l=legend('show');
    l.Interpreter='latex';
    xlabel('$\epsilon/T_c$','Interpreter','latex','FontSize',14)
    ylabel('$Cs$','Interpreter','latex','FontSize',14)
    title('Cs degradation for additional redundancy and rect pulse')
end