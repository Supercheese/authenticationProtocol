close all
x = 0:0.001:5;
alpha = 0.3;
beta = 0.7;
B = 1;
ni = alpha;
y = (alpha - alpha .* x) ./ (beta + B + (2 - 2*ni - beta).*x);
as = - alpha/(2 - 2*ni - beta) * ones(1,length(x));
figure
plot(x,y,x,as)
grid on
-(beta + B) / (2 - 2*ni - beta);

y1 = (alpha * x) ./ (B + 2 - 2*ni + (beta - 2 + 2 * ni) * x);
as1 = alpha / (beta - 2 + 2 * ni) * ones(1,length(x));
stem(x,y1,x,as1)