%% PSK channel, Eve's capacity

clear all
warning('error','MATLAB:integral2:nonFiniteResult');

sigmawstar=-6;%-16:20;
sigmawstarLin=db2pow(sigmawstar);
swsHalf=sigmawstarLin/2;

figure
hold on

for M=32%[2 4 8 16]
    xyInterval=50*[-1,1,-1,1];
    CE=zeros(size(sigmawstar));
    
    IO=[1 -1 1 -1];
    for z=1:length(swsHalf)
        fun=@(x,y) integrand(x,y,swsHalf(z),M);
        
        II=[-40 40 -40 40];
        success=0;
        while ~success
            try
                CE(z)=integral2(fun,II(1),II(2),II(3),II(4))-log2(2*pi*exp(1)*swsHalf(z));
                success=1;
            catch w
                II=II+IO;
            end
        end
        %     fsurf(fun,xyInterval)
    end
    p=plot(sigmawstar,CE);
    p.LineWidth=1.5;
    p.DisplayName=['M = ', num2str(M)];
    
end


grid on
l=legend('show');
l.FontSize=13;
xlabel('$\sigma_{w^*}^2 \ [dB]$','Interpreter','latex',...
    'FontSize',14)
ylabel('$C_E$','Interpreter','latex',...
    'FontSize',14)

warning('on','MATLAB:integral2:nonFiniteResult');