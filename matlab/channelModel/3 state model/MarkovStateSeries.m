%%       ------------------State Series Generation --------------------------------- 
function [ChannelStateSeries]=MarkovStateSeries
% uu_satcomsys_simu_parameters;
simu_parameters;
NoOfStateTransitions=round(RouteLength/LFrame1);    % No of state transitions
channel_state = zeros(1,NoOfStateTransitions);      % 3-state Markov chain (output vector).
channel_state(1) = randi([1 3],1,1);                % random initial state
cummadd_Prob_matrix = cumsum(Prob_matrix,2);        % cummulative probabilty state transition matrix
for i = 2:1:NoOfStateTransitions
    event = rand;                                   % uniform random number
    if channel_state(1,i-1) == 1 
        if event < cummadd_Prob_matrix(1,1)         % No switch
            channel_state(1,i) = 1;
        elseif event > cummadd_Prob_matrix(1,2)     % Switch to state 3
            channel_state(1,i) = 3;
        else
            channel_state(1,i) = 2;                 % Switch to state 2
        end
    elseif channel_state(1,i-1) == 2 
        if event < cummadd_Prob_matrix(2,1)         % Switch to state 1
            channel_state(1,i) = 1;
        elseif event > cummadd_Prob_matrix(2,2)     % Switch to state 3
            channel_state(1,i) = 3;
        else                                        % No switch
            channel_state(1,i) = 2;
        end
    elseif channel_state(1,i-1) == 3 
        if event < cummadd_Prob_matrix(3,1)         % Switch to state 1
            channel_state(1,i) = 1;
        elseif event > cummadd_Prob_matrix(3,2)     % No switch
            channel_state(1,i) = 3;
        else 
            channel_state(1,i) = 2;                 % Switch to state 2
        end; 
    end;
end;
%% ------------------------------Generation of Channel State series-----------------------------
ChannelStateSeries=[]; 
for ii=1:length(channel_state)
    if (channel_state(ii)==3), 
        ChannelStateSeries=[ChannelStateSeries; channel_state(ii); 3*ones(InterpRate3-1,1)];
    elseif (channel_state(ii)==2)
        ChannelStateSeries=[ChannelStateSeries; channel_state(ii); 2*ones(InterpRate2-1,1)];
    elseif  (channel_state(ii)==1)
        ChannelStateSeries=[ChannelStateSeries; channel_state(ii); ones(InterpRate1-1,1)];                 
    end  
end
