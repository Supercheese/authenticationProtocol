function [max_doppler_shift,Nsamples,fs,looaxis,loo_samples] =loo_series_twostates(M,S,MP)
Nsamples=20000;                             % approx.  number of samples to be generated 
freq= 2200e6;                               % carrier frequency (Hz)
velocity=0.83334;                           % MS speed (m/s)
Lcorr=1.5;                                  % Correlation distance (m) 
sample_factor=16;                           % Sampling factor of wavelength                     
sigma=sqrt(0.5*10^(MP/10));                 % convert to linear units 
AoA=90;                                     % angle of arrival of direct signal
lambdac=3e8/freq;                           % wavelength (m)
kc=2*pi/lambdac;                            % wave number  
max_doppler_shift=velocity/lambdac;         % Maximum Doppler Shift
ts=(lambdac/sample_factor)/velocity;        % sampling spacing (s)
fs=1/ts;                                    % sampling freq. (Hz)
          %% Generation of direct component samples 
sample_spacing=lambdac/sample_factor;
samplesLcorr=Lcorr/(sample_spacing); % No. of samples within Lcorr
samplesLcorr=round(samplesLcorr);           % Integer number: modifies slightly Lcorr
Nslowsamples=Nsamples/samplesLcorr;         % No of slow var. samples 
Nslowsamples=round(Nslowsamples);           % Integer number 
Nsamples=Nslowsamples*samplesLcorr;         % New total number of samples
slow=randn(Nslowsamples,1);                 % uncorrelated Gaussian slow variations
A=(slow*S)+M;                               % Normal distr.: Mean M and std S (ampl. dir. signal) 

%% Interpolate slow direct signal variations =============================
x=(0:Nslowsamples-1)*Lcorr;                 % axis in m (samples spaced Lcorr m)
x2=(0:Nsamples-1)*sample_spacing;
G2=interp1(x,A,x2,'spline');                % Interpolated amplitude for A 
G2ampl=10.^(G2/20); 
%% Direct signal's phase ===========================================
phi=kc.*(0:Nsamples-1)*lambdac./sample_factor*cosd(AoA);      % phase inccreases linearly with taveled distance 
phase_ini=2*pi*rand;                        % initial phase (drawn randmoly)
phi=phi+phase_ini;                          % overall phase of dir signal
%% overall complex direct signal ========================================
G2filt=G2ampl.*exp(1i*phi);
%% Generate fast variations 
I=randn(1,Nsamples);                        % In-phase component  
Q=randn(1,Nsamples);                        % Quadrature component 
ray=(I+1i*Q);
%%                          JAKES filter 
freqstep=5;
[filtertimedomain]=uu_satcomsys_jakes(freqstep,max_doppler_shift,fs);
%% Filtering of rayleigh samples 
ray_filt=sigma*conv(ray,filtertimedomain);
len=length(filtertimedomain);
ray_filt=ray_filt(len:end-len/2);
%% Loo samples
G2filt=G2filt(1:end-len/2);
Nsamples=Nsamples-len/2;
loo_samples=ray_filt+G2filt;
looaxis=(0:Nsamples-1)*lambdac/sample_factor;
end





