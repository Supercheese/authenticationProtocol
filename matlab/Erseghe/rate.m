clear all
close all
SNRBob=db2pow(5);
SNREve=db2pow(0);
Pe=1-1e-3; % target Pe given Psucc
nBar=250;

sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;

% Pe=3.3625e-03;
% n=250;
% R=C- sqrt(V/n)*log2(exp(1))*qfuncinv(Pe)+log2(n)/(2*n);


sigma=SNREve; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;


R=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2M=nBar*R;

for currn=1:nBar
    Rcurr=log2M/currn;
    Psucc(currn)=max(1-biAwgnF(SNREve,Rcurr,currn),1/(2^log2M));
end
semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB'])
grid on
hold on

%%

SNRBob=db2pow(5);
SNREve=db2pow(-5);
Pe=1-1e-3; % target Pe given Psucc
nBar=250;

sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;

% Pe=3.3625e-03;
% n=250;
% R=C- sqrt(V/n)*log2(exp(1))*qfuncinv(Pe)+log2(n)/(2*n);


sigma=SNREve; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;


R=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2M=nBar*R;

for currn=1:nBar
    Rcurr=log2M/currn;
    Psucc(currn)=max(1-biAwgnF(SNREve,Rcurr,currn),1/(2^log2M));
end
semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB'])
l=legend('show');
l.Interpreter='latex';
xlabel('n')
ylabel('P_{succ}')