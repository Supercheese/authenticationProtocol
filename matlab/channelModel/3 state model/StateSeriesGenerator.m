%   state duration statistics
function [channel_state_series]=StateSeriesGenerator(sigma_duration,alpha_duration,xmin,gamma,sample_space)
uu_satcomsys_input_parameters;
sigmadB=20*log10(sigma_duration);
alphadB=20*log10(alpha_duration);
gauss_duration=sigmadB*randn(1,Nsamples)+alphadB;
lognormal_duration=10.^(gauss_duration./10);
figure,plot(lognormal_duration);
[a,b]=uu_satcomsys_cdf(lognormal_duration);
bad_state_duration=round(a);
channel_stateseries=[];
power_law=xmin.*((1-rand(1,Nsamples)).^(-1/(gamma-1)));
[good_state_duration,b]=uu_satcomsys_cdf(power_law);
figure,plot(round(good_state_duration),b);
good_state_duration=round(good_state_duration/sample_space);
bad_state_duration=round(bad_state_duration/sample_space);
state_transitions=20;
channel_state = zeros(1,state_transitions); % 2-state Markov chain (output vector).
    channel_state(1) = 1;
for i = 2:1:state_transitions
    if(channel_state(1,i-1) == 1) 
         channel_state(1,i) = 2; 
    else
         channel_state(1,i) = 1;
    end
end
channel_state_series=[];
for ii=1:length(channel_state)
    if (channel_state(ii)==1), 
        channel_state_series=[channel_state_series; channel_state(ii);2*ones(good_state_duration(ii),1)];
    else
        channel_state_series=[channel_state_series; channel_state(ii);ones(bad_state_duration(ii),1)];  
    end
end
