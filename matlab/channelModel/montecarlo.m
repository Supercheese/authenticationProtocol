close all
clear all
addpath("C:\Users\Francesco\Documents\gitRepo\authenticationProtocol\matlab\bawgn")
figure(1)
param1 = [0.45 1.9 -16.9 -11.7 4.8 -21.3 -23.8 9.9 -22.5]; % tab 8 e 9 elev. 30�
W1 = [0.5051 0.1126 0.3823];
param2 = [-1 1 -15 -6 2.5 -17 -10.2 4 -15]; % tab 11 e 12 elev 60�
W2 = [0.0275 0.7611 0.214];
param3 = [-0.9 3 -9.1 -3.1 3.4 -9 -8 5 -7 ]; % tab 6 7 elev 80
W3 = [0.4167 0.25 0.333];

param = [param1; param2; param3];
W = [W1; W2; W3];
R = 0:0.001:10;
sigmaB = db2pow([-0 -5 -10]);
for k = 1:length(sigmaB)    
    [CDF_1,CDF_2,CDF_3,CDF_tot, snrBin] = computeOutage(param(1,:),W(1,:),R,sigmaB(k));
    gammaBin = 1./snrBin;
    gammaBin(gammaBin <= 0.001) = 0.001;
    gammaBin(gammaBin >= 400) = 400;
    for m = 1:length(gammaBin)
        Rbin(m) = bawgn(gammaBin(m));
    end
    p = loglog(R,CDF_1,Rbin,CDF_1);
    hold on
    p.LineWidth = 1.5;
    p.DisplayName = "$ \sigma_{w_B}^2 = " + num2str(pow2db(sigmaB(k))) + "$ dB";
end

ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui('center')
ax.FontSize=10;
grid on
xlabel("$R_x$",'Interpreter','latex','FontSize',14);
ylabel("$P_{out|S=1}$",'Interpreter','latex','FontSize',14);
l=legend('show');
l.Interpreter='latex';
l.FontSize=12;
l.Location='northwest';
ylim([0,1])

figure(2)

for k = 1:length(param(:,1))
    [CDF_1,CDF_2,CDF_3,CDF_tot] = computeOutage(param(k,:),W(k,:),R,sigmaB(3));
    p = loglog(R,CDF_tot);
    hold on
    
    p.LineWidth = 1.5;
    p.DisplayName = "Scenario " + num2str(k);
end

ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position=13 * [1 1 1 1];
movegui('center')
ax.FontSize=10;
grid on
xlabel("$R_x$",'Interpreter','latex','FontSize',14);
ylabel("$P_{out}$",'Interpreter','latex','FontSize',14);
l=legend('show');
l.Interpreter='latex';
l.FontSize=12;
l.Location='northwest';
ylim([0,1])
