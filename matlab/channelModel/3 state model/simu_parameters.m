%% Input parameters 
% Environment:Rural Intermediate Tree Shadowing(ITS)
elevation_angle=40;                         %  Elevation angle (in degrees)
RouteLength=100;                            %  Travelled distance by a mobile (in meters)
NFFT=512;                                   %  No of FFT points           
velocity=0.8333;                            %  speed of mobile (meters per sec)
Prob_matrix=[0.7826 0.1495 0.0679;          % State transition probability matrix
             0.2481 0.6899 0.062;
             0.2404 0.0601 0.6995];
SteadyState=[0.3929;0.3571;0.25];           % Probability being in a particular state
LFrame1=6.3;                                % minimum state1 duration in (meters)
LFrame2=6.3;                                % minimum state2 duration in (meters)
LFrame3=4.5;                                % minimum state3 duration in (meters)
freq=2200e6;                                % frequency in MHz      
lambdac=300e6/freq;                         % wavelength (in meters)
sampling_factor=16;                         % sampling fraction
sample_space=lambdac/sampling_factor;       % sampling spacing (in meters)
InterpRate1=round(LFrame1/sample_space);    % No of samples in state1 frame
InterpRate2=round(LFrame2/sample_space);    % No of samples in state2 frame
InterpRate3=round(LFrame3/sample_space);    % No of samples in state3 frame
Lcorr=1.5;                                  % Correlation distance (m) 
AoA=90;                                     % angle of arrival of direct signal
lambdac=3e8/freq;                           % wavelength (m)
kc=2*pi/lambdac;                            % wave number  
max_doppler_shift=velocity/lambdac;         % Maximum Doppler Shift
ts=(lambdac/sampling_factor)/velocity;      % sampling spacing (s)
fs=1/ts;                                    % sampling freq. (Hz)
sample_spacing=lambdac/sampling_factor;     % sampling period in meters 
Nsamples=round(RouteLength/sample_spacing); % No of samples in travelled distance
Nslowsamples=RouteLength/Lcorr;             % No of slow var. samples 
Nslowsamples=round(Nslowsamples);           % Integer number 
