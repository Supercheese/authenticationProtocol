%% Compute Bob's capacity given sigmawB and sigmawstar

clear all
warning('error','MATLAB:integral2:nonFiniteResult');

sigmawB=-16;
sigmawBlin=db2pow(sigmawB);
sigmawBhalf=sigmawBlin/2;
sigmawstar=-16:20;
sigmaTrueBob=db2pow(sigmawstar)+sigmawBlin;
stbHalf=sigmaTrueBob/2;

figure
hold on

for M=[2 4 8 16]
    xyInterval=50*[-1,1,-1,1];
    CB=zeros(size(sigmawstar));
    
    IO=[1 -1 1 -1];
    for z=1:length(stbHalf)
        fun=@(x,y) integrand(x,y,stbHalf(z),M);
        
        II=[-40 40 -40 40];
        success=0;
        while ~success
            try
                CB(z)=integral2(fun,II(1),II(2),II(3),II(4))-log2(2*pi*exp(1)*stbHalf(z));
                success=1;
            catch w
                II=II+IO;
            end
        end
        %     fsurf(fun,xyInterval)
    end
    p=plot(sigmawstar,CB);
    p.LineWidth=1.5;
    p.DisplayName=['M = ', num2str(M)];
    
end


grid on
l=legend('show');
l.FontSize=13;
xlabel('$\sigma_{w^*}^2 \ [dB]$','Interpreter','latex',...
    'FontSize',14)
ylabel('$C_B$','Interpreter','latex',...
    'FontSize',14)

warning('on','MATLAB:integral2:nonFiniteResult');