clear all
close all
nMax=488;
nMin=244;
k=244;

GammaE=db2pow(1);
z=1;
for n=nMin:nMax
    Pe(z)=normalApproxF(n,n/k,GammaE);
    z=z+1;
end

semilogy(nMin:nMax,Pe)
grid on
hold on

GammaE=GammaE- db2pow(-6);
z=1;
for n=nMin:nMax
    Pe(z)=normalApproxF(n,n/k,GammaE);
    z=z+1;
end
semilogy(nMin:nMax,Pe)
