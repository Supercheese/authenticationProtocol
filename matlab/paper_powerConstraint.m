close all
clear all
w_star = db2pow(-10:10);
w_Bval = db2pow([-10 -5 0]);
figure(1);
hold on
figure(2);
hold on
for w_B = w_Bval
    A =0.01:0.01:3;    
    w_star_max = min((A + w_B) / 2,A);
    C = log2(1 + (A - w_star_max) / w_B) - ...
        log2(1 + (A - w_star_max) / w_star_max );
    C = max(C,0);
    figure(1) %% questa inserita   
    p = plot(pow2db(A),C);
    p.DisplayName = "$\sigma^2_{w_B} = $" + num2str(pow2db(w_B)) + " dB";
    p.LineWidth = 1.5;
    n = 250;
    Psucc = 2.^(-C * n);
    figure(2)    
    p = plot(A,log10(Psucc));
    p.DisplayName = "$\sigma^2_{w_B} = $" + num2str(pow2db(w_B)) + " dB";
    p.LineWidth = 1.5;
%     figure(3)
%     hold on
%     p = plot(A,A-w_star_max);
end
figure(1)

ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui('center')
ax.FontSize=10;
grid on
xlabel("$A$ [dB]",'Interpreter','latex','FontSize',14)
ylabel("$C_A$ \ [b/s/Hz]",'Interpreter','latex','FontSize',14)
l=legend('show');
l.Interpreter='latex';
l.FontSize=12;
l.Location='northwest';
xlim([-7,4.7712])
% xlim([sigmawstar2(1) sigmawstar2(end)])
% 
% figure(2)
% grid on
% xlabel("$A$",'Interpreter','latex')
% ylabel("$P_{succ}$",'Interpreter','latex')
% ylim([-20,0])
% l = legend('show');
% l.Interpreter = 'latex';



% figure;
% plot(w_star,-w_star.^2 / w_B + (A / w_B + 1) * w_star)
% hold on
% scatter(w_star_max,C)