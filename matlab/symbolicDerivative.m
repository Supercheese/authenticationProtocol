clear all
close all

syms Gamma(e,A1,A2,B1,x,B,A)
Gamma = ((A1 + A2*e)^2 *x) / ((B1*e)^2 * x + B + 2*(A-x)*(1-A1-A2*e));
Gammap = diff(Gamma,e);
Gammapp = diff(Gammap,x);
CB = 0.5 * log2(1+Gamma);
CBp = diff(CB,x);

load rangingCodes.mat
T_c = 1;
c1 = c1 /sqrt(length(c1)*T_c);
mixedSum = c1(2:end)*c1(1:end-1)';
squareSum = sum(c1.^2);

A1 = squareSum*T_c;
A2 = (mixedSum - squareSum);
alpha = A1+A2*T_c/4;
B1 = c1(1)*c1(end);
B = db2pow(-5);
A = 20;
% e = 0:T_c/100:T_c;

figure
e = T_c/10;
beta = B1 * e;


[n,d] = numden(subs(Gammap));
Gammappp = (diff(n,x) * d - n * diff(d,x)) / d^2;
[n1, d1] = numden(Gammappp);
[n2,d2] = numden(subs(Gammapp));
fplot(subs(Gammap),[1,20])
p = coeffs(n2);
r = roots(fliplr(p));
a=1;

% lower bound
Rx = 0.5;
num = (1-2^(2*Rx)) * (B + 2*A - 2*A*alpha);
den = (1-2^(2*Rx)) * (beta^2 - 2 + 2*alpha) - alpha^2;
bound = num / den;
fplot(subs(Gammap),[0,5])
title("Gammap")
grid on

figure
fplot(subs(Gamma),[0,5])
title("Gamma")
grid on

figure
fplot(subs(Gammapp),[0,5])
title("Gammapp")
grid on
a=1;
% subs(Gammap,[e,A1,A2,B1,B2,x,B,omega],[1,2,3,4,5,6,7,8])
% CB = double(subs(log2(1+Gamma)));
% plot(e/T_c,double(subs(Gammap)))
% ylabel("$\frac{\partial \Gamma_B}{\partial \Delta_E}$", 'Interpreter','latex','FontSize',15)
% xlabel("$\frac{\Delta_E}{T_c} $", 'Interpreter','latex','FontSize',15)
% grid on
% 
% figure
% plot(e/T_c,double(subs(Gamma)))
% ylabel("$\Gamma_B$", 'Interpreter','latex','FontSize',15)
% xlabel("$\frac{\Delta_E}{T_c} $", 'Interpreter','latex','FontSize',15)
% grid on

% %% Maximize derivative
% 
% e = T_c/50;
% A = db2pow(2);
% x=0:A/100:A;
% 
% figure
% plot(x,double(subs(Gamma)))
% xlabel("$\sigma_x^2$", 'Interpreter','latex','FontSize',15)
% ylabel("$\Gamma_B(\sigma_x^2)$", 'Interpreter','latex','FontSize',15)
% title("$\Gamma_B(\sigma_x^2)$ fixed $\Delta_E$", 'Interpreter','latex','FontSize',15)
% grid on
% 
% figure
% plot(x,double(subs(Gammap)))
% xlabel("$\sigma_x^2$", 'Interpreter','latex','FontSize',15)
% ylabel("$\frac{\partial \Gamma_B}{\partial \Delta_E}(\sigma_x^2)$", 'Interpreter','latex','FontSize',15)
% title("$\frac{\partial \Gamma_B}{\partial \Delta_E}(\sigma_x^2)$ fixed $\Delta_E$", 'Interpreter','latex','FontSize',15)
% grid on
% 
% figure
% plot(x,double(subs(Gammapp)))
% xlabel("$\sigma_x^2$", 'Interpreter','latex','FontSize',15)
% ylabel("$\frac{\partial^2 \Gamma_B}{\partial \Delta_E \partial \sigma^2_x}(\sigma_x^2)$", 'Interpreter','latex','FontSize',15)
% title("$\frac{\partial^2 \Gamma_B}{\partial \Delta_E \partial \sigma^2_x}(\sigma_x^2)$ fixed $\Delta_E$", 'Interpreter','latex','FontSize',15)
% grid on
% 
% %% maximize capacity
% 
% figure
% plot(x,double(subs(CB)))
% xlabel("$\sigma_x^2$", 'Interpreter','latex','FontSize',15)
% ylabel("$C_B(\sigma_x^2)$", 'Interpreter','latex','FontSize',15)
% title("$C_B(\sigma_x^2)$ fixed $\Delta_E$", 'Interpreter','latex','FontSize',15)
% grid on
% 
% figure
% plot(x,double(subs(CBp)))
% xlabel("$\sigma_x^2$", 'Interpreter','latex','FontSize',15)
% ylabel("$\frac{\partial C_B}{\partial \sigma_x^2}(\sigma_x^2)$", 'Interpreter','latex','FontSize',15)
% title("$\frac{\partial C_B}{\partial \sigma_x^2}(\sigma_x^2)$ fixed $\Delta_E$", 'Interpreter','latex','FontSize',15)
% grid on
