GammaDb=0:.1:2;
Gamma=db2pow(GammaDb);
k=1024;
n=2048;

for z=1:length(Gamma)
    R=k/n;
    C=.5*log2(1+Gamma(z));
    V=Gamma(z)*(2+Gamma(z))/(2*(1+Gamma(z))^2);
    Pe(z)=qfunc(sqrt(n/V)*( (C-R)/log2(exp(1)) + log(n)/(2*n)));
end

semilogy(GammaDb,Pe)
ylim([1e-7 1e-2])
xlim([0 2])
grid on