%% Complex Gaussian channel. Secrecy capacity.

SNRBob=16; %dopo il despreading
sigmawB2=-SNRBob;
sigmawstar2=sigmawB2:30;
SNREve=-sigmawstar2;
SNRBoblin=db2pow(SNRBob);
SNREvelin=db2pow(SNREve);

Cs=log2(1+SNRBoblin)-log2(1+SNREvelin);

figure
plot(sigmawstar2,Cs)
