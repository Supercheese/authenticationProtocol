clear all
load equivTapsTwoHornDelay
pulsetype='2Horn';
plots=0;

Nc=4092; % spreading seq length

% Noise
sigmawstarChip=100; % w*
sigmawstar=db2pow(-6); % symbol noise w*
SNRbob=16; %dB after despreader
sigmawB=db2pow(-SNRbob); % symbol noise w_B given unit energy pulse
sigmaTot=sigmawB;%+sigmawstar; % total symbol noise

M=8;
h=comm.LDPCEncoder;
hdec=comm.LDPCDecoder('IterationTerminationCondition','Parity check satisfied');
pskMod=comm.PSKModulator('PhaseOffset',0,'BitInput',1,'ModulationOrder',M);
pskDemod=comm.PSKDemodulator('PhaseOffset',0,'BitOutput',1,...
    'DecisionMethod','Log-likelihood ratio', 'Variance',sigmaTot...
    ,'ModulationOrder',M);
H=h.ParityCheckMatrix;
n=length(H(1,:));
k=n-length(H(:,1));

punct=round(0.03*n);
mask4=logical([ones(n-punct,1); zeros(punct,1)]);
authBit=round(k*(2/20)); % number of authenticated bits
R=k/(k+authBit);
infR=log2(M)*R;
mask1=logical([ones(k,1);zeros(n-k,1)]); % mask for tx bit
mask2=logical([zeros(k,1); ones(authBit,1);zeros(n-k-authBit,1)]); %mask for auth bit
mask3=logical([zeros(k+authBit,1); ones(n-k-authBit,1)]); % mask for deleted bits

%% simulation
addRed=0; % toggle for sigmawstar removal or additional redundancy
if addRed==0
    mask1(:)=true;
end

delay=1:10:101;

Nit=1e2;
nerr=zeros(Nit,length(delay));
npack=zeros(Nit,length(delay));
tic

parfor it=1:Nit
    if mod(it,1e3)==0
        it
    end

    % secret message
    u=double(randn(k,1)>0);
    
    % encode
    c=step(h,u);
    
    %set up llrs
    llr=zeros(n,1);
    if addRed
        llr(mask2)=inf;
        llr(c>0)=-inf; %set authenticated bits
        llr(mask3)=0; %lost bits
        % send only k bits. The rest is auth channel
        cSent=c(1:k);
    else
        cSent=c(1:end-punct);        
    end
    
    % modulate
    ak=step(pskMod,cSent);
    ns=length(ak); % number of txmitted symbols
    akHat=zeros(1,ns);
    
    %noise
    wB=sqrt(sigmaTot/2)*(randn(1,ns)+1j*randn(1,ns));
    
    nerrfor=zeros(1,length(delay));
    npackfor=zeros(1,length(delay));
    % channel
    for d=1:length(delay)
        
        for z=1:ns-1
            akHat(z)=alpha(delay(d))*ak(z)+beta(delay(d))*ak(z+1);
        end
        akHat(ns)=alpha(delay(d))*ak(ns); %last symbol
        
        %noise
        rB=akHat+wB;
        
        %demodulate
        if addRed==1
            llr(mask1)=step(pskDemod,rB.'); % llr
        else
            llr(mask4)=step(pskDemod,rB.');
        end
        
        %decode
        uHat=step(hdec,llr);
        
        check=sum(u~=uHat);
        nerrfor(d)=nerrfor(d)+check;
        if check~=0
            npackfor(d)=npackfor(d)+1;
        end
    end
    nerr(it,:)=nerrfor;
    npack(it,:)=npackfor;
end
toc
accuracy=10/(Nit*k);
nerr=sum(nerr,1);
npack=sum(npack,1);
Pbit=nerr/(k*Nit);
Cer=npack/Nit;

delay=delay/100;
if plots
    figure
    semilogy(delay,Pbit)
    grid on
    xlabel('$\epsilon/T_c$','Interpreter','latex')
    ylabel('$P_{bit}$','Interpreter','latex')
    title([pulsetype ' $\sigma_w^*=$' num2str(pow2db(sigmawstar)) ' [dB]' ],...
        'Interpreter', 'Latex')
    xlim([0,1]);
end