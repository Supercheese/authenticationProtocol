function [ray_filt] = raysim(L,ctf,fs)

wn = 2*ctf/fs;              %Normalized cutoff frequency
[b,a] = butter(3,wn);       %3-pole Butterworth polynomials
Q=filter(b,a,randn(1,L));   %Filtering of gaussian quadrature component 
I=filter(b,a,randn(1,L));   %Filtering of gaussian inphase component
ray_filt = I+1i*Q;          %sum of Gaussian inphase and Gaussian quadrature components 
