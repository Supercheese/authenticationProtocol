%% simulazione al ricevitore di EVE con zero ritardi

clear all
close all
load equivTapsOneHornTrueDelay
pulsetype='rect';
plots=0;

Nc=4092; % spreading seq length

% Noise
sigmawstarChip=100; % w*
sigmawstar=db2pow(-6); % symbol noise w*
SNRbob=16; %dB after despreader
sigmawB=db2pow(-SNRbob); % symbol noise w_B given unit energy pulse
sigmaTot=sigmawstar; % total symbol noise

M=8;
h=comm.LDPCEncoder;
hdec=comm.LDPCDecoder('IterationTerminationCondition','Parity check satisfied');
pskMod=comm.PSKModulator('PhaseOffset',0,'BitInput',1,'ModulationOrder',M);
pskDemod=comm.PSKDemodulator('PhaseOffset',0,'BitOutput',1,...
    'DecisionMethod','Log-likelihood ratio', 'Variance',sigmaTot...
    ,'ModulationOrder',M);
H=h.ParityCheckMatrix;
n=length(H(1,:));
k=n-length(H(:,1));
mask1=logical([ones(k,1);zeros(n-k,1)]); % mask for tx bit
punct=round(0.05*(n-k)); %punctured bit
mask2=logical(zeros(n,1)); % mask for punct bits

%select random punct
set=k+1:n;
setLen=n-k; % size of array
for z=1:punct
    currIdx=randi(setLen);
    mask2(set(currIdx))=true;
    % resize array
    for z1=currIdx+1:setLen
        set(z1-1)=set(z1);
    end
    setLen=setLen-1;
end
mask3=logical([zeros(k,1);~mask2(k+1:end)]); %mask for auth bits
R1=log2(M);
R2=log2(M)*k/(n-punct);

%% simulation

delay=1; 

Nit=1e2;
nerr=zeros(Nit,length(delay));
npack=zeros(Nit,length(delay));
tic
parfor it=1:Nit
    if mod(it,1e3)==0
        it
    end

    % information bits
    u=double(randn(k,1)>0);
    
    % encode
    c=step(h,u);
    
    %set up llrs
    llr=zeros(n,1);
    %     llr(mask3)=inf;
    %     llr(c>0)=-inf; %set authenticated bits
    %     llr(mask2)=0; %lost bits
    %
    % send only k bits. The rest is auth channel
    cSent=c(1:k);
    
    % modulate
    ak=step(pskMod,cSent);
    ns=length(ak); % number of txmitted symbols
    akHat=zeros(1,ns);
    
    %noise
    wB=sqrt(sigmaTot/2)*(randn(1,ns)+1j*randn(1,ns));
    
    nerrfor=zeros(1,length(delay));
    npackfor=zeros(1,length(delay));
    % channel
    %     for d=1:length(delay)
    d=delay;
    for z=1:ns-1
        akHat(z)=alpha(delay(d))*ak(z)+beta(delay(d))*ak(z+1);
    end
    akHat(ns)=alpha(delay(d))*ak(ns); %last symbol
    
    %noise
    rB=akHat+wB;
    
    %demodulate
    llr(mask1)=step(pskDemod,rB.'); % llr
    
    %decode
    uHat=step(hdec,llr);
    
    check=sum(u~=uHat);
    nerrfor(d)=nerrfor(d)+check;
    if check~=0
        npackfor(d)=npackfor(d)+1;
    end
    %     end
    nerr(it,:)=nerrfor;
    npack(it,:)=npackfor;
end
toc
accuracy=10/(Nit*k);
nerr=sum(nerr,1);
npack=sum(npack,1);
Pbit=nerr/(k*Nit);
Cer=npack/Nit;

delay=delay/100;
if plots
    figure
    semilogy(delay,Pbit)
    grid on
    xlabel('$\epsilon/T_c$','Interpreter','latex')
    ylabel('$P_{bit}$','Interpreter','latex')
    title([pulsetype ' $\sigma_w^*=$' num2str(pow2db(sigmawstar)) ' [dB]' ],...
        'Interpreter', 'Latex')
    xlim([0,1]);
    
    figure
    semilogy(delay,Cer)
    grid on
    xlabel('$\epsilon/T_c$','Interpreter','latex')
    ylabel('$Codeword Error Rate$','Interpreter','latex')
    title([pulsetype ' $\sigma_w^*=$' num2str(pow2db(sigmawstar)) ' [dB]' ],...
        'Interpreter', 'Latex')
    xlim([0,1]);
end