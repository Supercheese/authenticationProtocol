
function C =  bawgn(sigma2)

integrand = @(y) 1/(sqrt(8*pi*sigma2)) * (exp(-(y-1).^2/(2*sigma2)) + exp(-(y+1).^2/(2*sigma2))) ...
    .* log2(1/(sqrt(8*pi*sigma2)) * (exp(-(y-1).^2/(2*sigma2)) + exp(-(y+1).^2/(2*sigma2))));
% fplot(integrand,[-20,20])
if sigma2 >=20
    integr = 100;
elseif sigma2 >= 10
    integr = 30;
elseif sigma2 >= db2pow(0)
    integr = 20;
elseif sigma2 >= db2pow(-20)
    integr = 5;
elseif sigma2 >= db2pow(-30)
    integr = 2;
else
    integr = 0.5;
end
C = -integral(integrand,-integr,integr) - 0.5 * log2(2 * pi * exp(1) * sigma2);

end