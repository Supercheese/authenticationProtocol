%% Compute SNR (and Cs) degradation due to interference given by delay attack. Build differe
%nt pulses

clear all
close all
PSK=1;
skip = 0;
generate = 0;
addpath("bawgn")
addpath("Erseghe")
% shape='2hornStandard';
shape = 'standard';

%SNR degradation
nSamp=1000;% samples per pulse (continuos time approx)
hadamard=0; % want Hadamard or standard?
if hadamard
    Ns=2^12; % length of spreading seq
    A=hadamard(Ns);
    ss1=A(1011,:);
else
    load rangingCodes.mat
    ss1=c4;
    Ns=length(c4);
end
tt=nSamp*Ns; %total taps for one symbol

% receiver matched filter
receiverRect=0; %if =0 use standard pulse
if receiverRect==1
    g=rectpulse(ss1,nSamp); %one symbol period
    g=g/sqrt(g*g');% unit energy on T_s
else
    g=buildPulse(nSamp,0,0,ss1,"standard");
    g=g/sqrt(g*g');% unit energy on T_s
end

%transmission shape
if shape=="rect"
    x=rectpulse(ss1,nSamp); %one symbol period
    x=x/sqrt(x*x');% unit energy on T_s    
elseif shape=="horn"    
    x=buildPulse(nSamp,30,30,ss1,shape);
    x=x/sqrt(x*x');% unit energy on T_s
elseif shape=="standard"
    x=buildPulse(nSamp,0,0,ss1,shape);
    x=x/sqrt(x*x');% unit energy on T_s
elseif shape=="1hornStandard"
    x=buildPulse(nSamp,0,0,ss1,shape);
    x=x/sqrt(x*x');% unit energy on T_s
elseif shape=="2hornStandard"
    x=buildPulse(nSamp,0,0,ss1,shape);
    x=x/sqrt(x*x');% unit energy on T_s
end

%% SNR definitions
sigmawB2=db2pow(-5);
SNREve=1/db2pow(0); % SNR Eve
sigmawstar=1/SNREve;

%%
delMax=nSamp;
delay=0:delMax; % in number of taps
rB=x;
g=rB; %use matched filter

%% delays
alpha = zeros(1,length(delay));
beta = zeros(1,length(delay));
nu = zeros(1,length(delay));
sigr = zeros(1,length(delay));
SNRBobDel = zeros(1,length(delay));
SNRBobAnt = zeros(1,length(delay));
tic
if generate
    parfor dl=1:length(delay) % taps
        rBd=circshift(rB,-delay(dl));
        %despreading
        alpha(dl)=rBd(1:tt-delay(dl))*g(1:tt-delay(dl))';
        beta(dl)=rBd(tt-delay(dl)+1:end)*g(tt-delay(dl)+1:end)';
        nu(dl)=rB(1:end-delay(dl))*rB(delay(dl)+1:end)';
        sigr(dl)=2*sigmawstar*(1-nu(dl));
        SNRBobDel(dl)=alpha(dl)^2/(sigmawB2+beta(dl)^2+sigr(dl));
    end
    % save('equivTapsOneHornDelay','alpha','beta','SNRBobDel')
    
    %% anticipate
    rB=x;
    parfor dl=2:length(delay) % taps
        rBd=circshift(rB,+delay(dl));
        %despreading
        alpha=rBd(delay(dl)+1:end)*g(delay(dl)+1:end)';
        beta=rBd(1:delay(dl))*g(1:delay(dl))';
        
        SNRBobAnt(dl)=alpha^2/(sigmawB2+beta^2);
    end
    toc
    SNRBob=[flip(SNRBobDel(2:end)) SNRBobDel];
    delay=(-delMax:delMax)/nSamp;
    save(['snrBob_' shape],'delay','SNRBob')
else
    str = ['snrBob_' shape];
    load(str)
end
if ~skip
    CsDegr=.5*(log2(1+SNRBob));%-log2(1+SNREve));
    CsDegr(CsDegr<0)=0;
    
    
    % save('SNRbob1hornStandardDelAnt','delay','SNRBob')
    
    %% plot of Cs deg Gaussian
    n = 250;
    f=figure;
    p=plot(delay,CsDegr);
    p(1).LineWidth=1.5;
    p(1).Color='k';
    ax=gca;
    ax.PlotBoxAspectRatio=[1 1 1];
    f.Units='centimeters';
    f.Position=[10 10 10 10];
    movegui(f,'center')
    ax=gca;
    ax.FontSize=10;
    grid on
    xlabel('$\Delta_E/T_c$','FontSize',14,'FontWeight','Bold','Interpreter','latex')
    ylabel('$C_B$ [b/s/Hz]','FontSize',14,'FontWeight','Bold','Interpreter','latex')
    %ylim([0,1.6])
    xlim([-1,1])
    % legend({'$C_s$','$\frac{1}{2} \log_2 (1+\Gamma_B)$'},'FontSize',12,'Interpreter','latex')
    pbaspect([1 1 1])
    
    if PSK
        %% plot Psk capacity
        
        warning('error','MATLAB:integral2:nonFiniteResult');
        sigmawB=1./SNRBob;
        sigmawBhalf=sigmawB/2; %from loaded data
        
        sigmawstarhalf=sigmawstar/2;
        
        pos=sigmawBhalf<sigmawstarhalf;
        sigmawBsim=sigmawB;%(pos);
        sigmawBhalfsim=sigmawBhalf;%(pos);
        Cs=zeros(size(sigmawB));
        
        f=figure;
        pbaspect([1 1 1])
        grid on
        hold on
        % for M=[2]% 32 64]
        %
        %     fun=@(x,y) integrand(x,y,sigmawstarhalf,M);
        %     CE=integral2(fun,-8,8,-8,8)-log2(2*pi*exp(1)*sigmawstarhalf);
        % %        fsurf(fun)
        %
        %     CB=zeros(size(sigmawBsim));
        %     xyInterval=50*[-1,1,-1,1];
        %     IO=[1 -1 1 -1];
        %     for z=1:length(sigmawBsim)
        %         fun=@(x,y) integrand(x,y,sigmawBhalfsim(z),M);
        % %             fsurf(fun,xyInterval)
        %         II=[-3 3 -3 3];
        %         success=0;
        %         while ~success
        %             try
        %                 CB(z)=integral2(fun,II(1),II(2),II(3),II(4))-log2(2*pi*exp(1)*sigmawBhalfsim(z));
        %                 success=1;
        %             catch w
        %                 II=II+IO;
        %             end
        %         end
        %     end
        %     Cs=CB;%-CE;
        %     Cs(Cs<0)=0;
        %     p=plot(delay,Cs);
        %     p.LineWidth=1.5;
        %     p.DisplayName=['M = ' num2str(M)];
        % %     p.Color='k';
        % end
        
        for z=1:length(sigmawBsim)
            Cs(z) = bawgn(sigmawBsim(z));
        end
        
        Cs(Cs<0)=0;
        p=plot(delay,Cs);
        p.LineWidth=1.5;
        p.DisplayName=["Binary"];
        
        ax=gca;
        ax.PlotBoxAspectRatio=[1 1 1];
        f.Units='centimeters';
        f.Position=13 * [1 1 1 1];
        movegui(f,'center')
        
        % xlim([-0.1 0.1])
        
        % Gaussian case
        
        
        CsG=0.5*log2(1+SNRBob);%-log2(1+SNREve);
        CsG(CsG<0)=0;
        p=plot(delay,CsG,'k');
        p.LineWidth=1.5;
        p.DisplayName='Gaussian';
        l=legend('show');
        
        grid on
        ax=gca;
        ax.FontSize=10;
        xlabel('$\Delta_E/T_c$','Interpreter','latex','FontSize',14)
        ylabel('$C_B''$ [b/s/Hz]','Interpreter','latex','FontSize',14)
        l.FontSize=12;
        p=get(gca,'Children');
        p(1).LineStyle='-';
        p(2).LineStyle='--';
        % p(3).LineStyle='-.';
        % p(4).LineStyle=':';
        
        warning('on','MATLAB:integral2:nonFiniteResult');
    end
end

%% Finite length

%Gaussian
figure
nBar = 100;
for R = [0.05 0.2 0.4]
    for k = 1:length(SNRBob)
        Psucc(k)=1-normalApproxF(R * nBar,nBar, SNRBob(k)); %compute Eve's Pe
    end
    p=semilogy(delay,Psucc);
    p.DisplayName = ['$R_x=' num2str(R) '$'];
    p.LineWidth = 1.4;
    hold on
end

ylim([1e-10 1])
ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui('center')
ax.FontSize=10;
grid on
xlabel("$\Delta_E/T_c$",'Interpreter','latex','FontSize',14);
ylabel("$1-q$",'Interpreter','latex','FontSize',14);
l = legend("show");
l.Interpreter = 'latex';
l.FontSize=12;
grid on