clear all
close all
SNRBob=db2pow(0);
SNREve=db2pow(-5);
Pe=1e-3; % target Pe for Bob
nBar=250;

sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;


R1=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2gamma=R1*nBar;

sigma=SNREve; %just change names

for currn=1:nBar
    Rcurr=log2gamma/currn;
    Psucc(currn)=max(1-biAwgnF(SNREve,Rcurr,currn),1/(2^log2gamma));
end
p=semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB']);
p.LineWidth=1.5;
p.Color='k';
grid on
hold on

%%
SNRBob=db2pow(0);
SNREve=db2pow(-1);

sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;

R2=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2gamma=R2*nBar;
sigma=SNREve; %just change names

for currn=1:nBar
    Rcurr=log2gamma/currn;
    Psucc(currn)=max(1-biAwgnF(SNREve,Rcurr,currn),1/(2^log2gamma));
end
p=semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB']);
p.LineWidth=1.5;
p.LineStyle='-.';
p.Color='k';

l=legend('show');
l.Interpreter='latex';
xlabel('$n$','Interpreter','latex')
ylabel('$P_{succ}$','Interpreter','latex')


%%
SNRBob=db2pow(0);
SNREve=db2pow(0);
Pe=1e-3; % target Pe for Bob
nBar=250;

sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;

R1=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2gamma=R1*nBar;

sigma=SNREve; %just change names

for currn=1:nBar
    Rcurr=log2gamma/currn;
    Psucc(currn)=max(1-biAwgnF(SNREve,Rcurr,currn),1/(2^log2gamma));
end
p=semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB']);
p.LineWidth=1.5;
p.LineStyle='--';
p.Color='k';
grid on
hold on

%%
SNRBob=db2pow(0);
SNREve=db2pow(5);
Pe=1e-3; % target Pe for Bob
nBar=250;

sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;

R1=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2gamma=R1*nBar;

sigma=SNREve; %just change names

for currn=1:nBar
    Rcurr=log2gamma/currn;
    Psucc(currn)=max(1-biAwgnF(SNREve,Rcurr,currn),1/(2^log2gamma));
end
p=semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB']);
p.LineWidth=1.5;
p.LineStyle=':';
p.Color='k';
grid on
hold on

f=figure(1);
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui(f,'center')
ax=gca;
ax.FontSize=10;

ylim([1e-15 2e0])
xlim([50,250])
l=legend('show');
l.Interpreter='latex';

xlabel('$n$','Interpreter','latex','FontSize',14)
ylabel('$P_{succ}$','Interpreter','latex','FontSize',14)
l.FontSize=12;