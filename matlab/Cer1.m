% rect psk=8

  Cer=[0 0 0 0 0.19720 1.0000 1.0000 ...
    1.0000    1.0000    1.0000    1.0000];
semilogy((0:10:100)/100,Cer)
xlabel('$\epsilon / T_c$','Interpreter','latex')
ylabel('Codeword error rate','Interpreter','latex')
grid on
