clear all

SNRBob=db2pow(10);
SNREve=db2pow(6);


% R=log2(M)/n
CB=.5*log2(1+SNRBob);
R=CB;
nBar=50;
log2M=nBar*R;
log10(1-normalApproxF(log2M,nBar,SNREve));
normalApproxF(log2M,nBar,SNREve)

z=1;
 for n=1:.5:nBar
    oneMPe(z)=1-normalApproxF(log2M,n,SNREve);
    z=z+1;
 end
 
 semilogy(1:z-1,oneMPe)
 grid on
%  xlim([1,z-1])
 xlabel("$n$",'Interpreter','latex')
 ylabel("$1-P_{EVE}$",'Interpreter','latex')
 
 Psucc=(1/2^log2M)*(1-oneMPe) + oneMPe;
 figure
 semilogy(1:z-1,Psucc,'LineWidth',2)
 grid on
 
 xlabel("$n$",'Interpreter','latex','FontSize',15)
 ylabel("$P_{ succ}$",'Interpreter','latex','FontSize',15)