clear all
close all
others=0; %plot only uniform
%% Uniform quantizer, Gaussian input

SNRBob=5; %dopo il despreading
sigmawB2=-SNRBob;
sigmawstar2=sigmawB2:10;
SNREve=db2pow(-sigmawstar2);
n = 250;

sigmaq2=-[4.40 9.25 14.27 19.38 24.57 29.83 35.13 40.34];

f=figure;
bVal=1:3;
CsUnif=zeros(length(bVal),length(sigmawstar2));

for b=bVal
    SNRBob=1/(db2pow(sigmawB2)+db2pow(sigmaq2(b)));
    Cs=.5*(log2(1+SNRBob)-log2(1+SNREve));
    Cs(Cs<0)=0;
    CsUnif(b,:)=Cs;
%     p=semilogy(sigmawstar2,2.^(-Cs*n));
    p=plot(sigmawstar2,Cs);
    hold on
    p.LineWidth=1.5;
    p.DisplayName=['b= ' num2str(b)];
    %     p.Color='k';
end
grid on



SNRBob=1/(db2pow(sigmawB2));
CsRem=.5*(log2(1+SNRBob)-log2(1+SNREve));
CsRem(CsRem<0)=0;
% p=semilogy(sigmawstar2,2.^(-CsRem*n));
p=plot(sigmawstar2,CsRem);
p.LineWidth=1.5;
p.DisplayName='$\rm b=\infty$';
p.Color='k';
f.Units='centimeters';
f.Position=[10 10 10 10];

ax=gca;
ax.FontSize=10;
xlabel("$\sigma_{\omega}^2$ [dB]",'Interpreter','Latex','FontSize',14)
% ylabel("$P_{pred}$",'Interpreter','Latex','FontSize',14)
ylabel("$C_s$",'Interpreter','Latex','FontSize',14)
ylim([1e-20,1])
% title(['$\sigma_{w_B}^2=$', num2str(sigmawB2),' dB'], 'Interpreter','latex');


l=legend('show');
l.Interpreter='latex';
l.Location='northeast';
l.FontSize=12;

p=get(gca,'Children');
p(1).LineStyle='-';
p(1).Color='k';
p(2).LineStyle='--';
% p(2).Color='k';
p(3).LineStyle='-';
p(3).Marker='o';
p(4).Marker='d';
% p(1).Color='k';
movegui('center')

if others
    %% Non uniform optimized quantizer b=3 Gaussian input
    figure;
    hold on
    
    sigmaq2=[0.363 0.117 0.0345 0.00955];
    bVal=1:4;
    CsOptGauss=zeros(length(bVal),length(sigmawstar2));
    for b=bVal
        SNRBob=1/(db2pow(sigmawB2)+sigmaq2(b));
        Cs=.5*(log2(1+SNRBob)-log2(1+SNREve));
        Cs(Cs<0)=0;
        CsOptGauss(b,:)=Cs;
        p=plot(sigmawstar2,Cs,sigmawstar2,CsUnif(b,:));
        p(1).LineWidth=1.5;
        p(1).DisplayName=['Non uniform, b=' num2str(b)];
        p(2).LineWidth=1.5;
        p(2).DisplayName=['Uniform, b=' num2str(b)];
        p(1).Color=p(2).Color;
        p(2).LineStyle='--';
    end
    
    p=plot(sigmawstar2,CsRem);
    p.LineWidth=1.5;
    p.DisplayName='No quantization';
    p.Color='k';
    
    
    grid on
    legend('show')
    xlabel("$\sigma_{w^*}^2 \ [dB]$",'Interpreter','Latex','FontSize',13.5)
    ylabel("$C_s$ [bit/s/Hz]",'Interpreter','Latex','FontSize',13.5)
    title('Non uniform, optimized quantizer. Gaussian Input')
    
    %% Uniform input Uniform quantizer
    sigmaq2=-[6.02 12.04 18.06 24.08 30.10 36.12 42.14 48.17];
    figure
    hold on
    bVal=1:8;
    CsUnifInput=zeros(length(bVal),length(sigmawstar2));
    
    for b=bVal
        SNRBob=1/(db2pow(sigmawB2)+db2pow(sigmaq2(b)));
        Cs=.5*(log2(1+SNRBob)-log2(1+SNREve));
        Cs(Cs<0)=0;
        CsUnifInput(b,:)=Cs;
        p=plot(sigmawstar2,Cs);
        p.LineWidth=1.5;
        p.DisplayName=['b= ' num2str(b)];
    end
    grid on
    legend('show')
    
    SNRBob=1/(db2pow(sigmawB2));
    CsRem=.5*(log2(1+SNRBob)-log2(1+SNREve));
    CsRem(CsRem<0)=0;
    p=plot(sigmawstar2,CsRem);
    p.LineWidth=1.5;
    p.DisplayName='Noise removal';
    p.Color='k';
    
    xlabel("$\sigma_{w^*}^2$",'Interpreter','Latex','FontSize',13.5)
    ylabel("$C_s$ [bit/s/Hz]",'Interpreter','Latex','FontSize',13.5)
    title('Uniform input uniform optimized quantizer')
    %% Confronto Uniform and Gaussian
    b=3;
    figure
    hold on
    p=plot(sigmawstar2,CsUnifInput(b,:),...
        sigmawstar2,CsOptGauss(b,:),...
        sigmawstar2,CsRem);
    for k=1:3
        p(k).LineWidth=1.5;
    end
    p(3).Color='k';
    legend('Uniform input','Gaussian input','Noise cancellation');
    xlabel("$\sigma_{w^*}^2$",'Interpreter','Latex','FontSize',13.5)
    ylabel("$C_s$ [bit/s/Hz]",'Interpreter','Latex','FontSize',13.5)
    grid on
    
end