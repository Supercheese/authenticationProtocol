function [CDF_1,CDF_2,CDF_3,CDF_tot,snrBin] = computeOutage(param,W,R,sigmaB)
alpha_1 = param(1);
psi_1 = param(2);
MP_1 = param(3);
alpha_2 = param(4);
psi_2 = param(5);
MP_2 = param(6);
alpha_3 = param(7);
psi_3 = param(8);
MP_3 = param(9);

b0_1 = 0.5 * 10^(MP_1/10);
mu_1 = log(10^(alpha_1/20));
d0_1 = (log(10^(psi_1/20)))^2;
b0_2 = 0.5 * 10^(MP_2/10);
mu_2 = log(10^(alpha_2/20));
d0_2 = (log(10^(psi_2/20)))^2;
b0_3 = 0.5 * 10^(MP_3/10);
mu_3 = log(10^(alpha_3/20));
d0_3 = (log(10^(psi_3/20)))^2;

N = 1e6;

logNormal_1 = exp(sqrt(d0_1) * randn(1,N) + mu_1);
phi0_1 = 2*pi*rand(1,N);
ray_1 = sqrt(b0_1) * (randn(1,N) + 1j * randn(1,N));
r = logNormal_1 .* phi0_1 + ray_1;
r_square_1 = abs(r).^2 /mean(abs(r).^2);

logNormal_2 = exp(sqrt(d0_2) * randn(1,N) + mu_2);
phi0_2 = 2*pi*rand(1,N);
ray_2 = sqrt(b0_2) * (randn(1,N) + 1j * randn(1,N));
r = logNormal_2 .* phi0_2 + ray_2;
r_square_2 = abs(r).^2 /mean(abs(r).^2);

logNormal_3 = exp(sqrt(d0_3) * randn(1,N) + mu_3);
phi0_3 = 2*pi*rand(1,N);
ray_3 = sqrt(b0_3) * (randn(1,N) + 1j * randn(1,N));
r = logNormal_3 .* phi0_3 + ray_3;
r_square_3 = abs(r).^2 /mean(abs(r).^2);



CDF_1 = zeros(1,length(R));
CDF_2 = zeros(1,length(R));
CDF_3 = zeros(1,length(R));
CDF_tot = zeros(1,length(R));
snrBin = zeros(1,length(R));
for k=1:length(R)    
    outage_1 = r_square_1 <= (2^(2 * R(k)) - 1) * sigmaB;
    outage_2 = r_square_2 <= (2^(2 * R(k)) - 1) * sigmaB;
    outage_3 = r_square_3 <= (2^(2 * R(k)) - 1) * sigmaB;
    snrBin(k) = (2^(2 * R(k)) - 1);    
    CDF_1(k) = sum(outage_1) / length(outage_1);
    CDF_2(k) = sum(outage_2) / length(outage_1);
    CDF_3(k) = sum(outage_3) / length(outage_1);
    CDF_tot(k) = CDF_1(k) * W(1) + CDF_2(k) * W(2) + CDF_3(k) * W(3);    
end
end

