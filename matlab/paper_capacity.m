%% Plot of Secrecy capacity versus w*, Gaussian case
close all
clear all
Ns=4092; % SS length
SNRBobMax=10;
sigmawB2min=-SNRBobMax;
sigmawstar2=sigmawB2min-3:5;
SNREve=-sigmawstar2;
SNREvelin=db2pow(SNREve);

f=figure;
lineBin={'-.','--',':'};
lineGauss={'-.d','--d',':d'};

k=1;
n = 250;
for SNRBob=0:5:10 %dopo il despreading
    sigmawB2=-SNRBob;
    SNRBoblin=db2pow(SNRBob);  
    addpath('bawgn')
    CBbin = bawgn(1/SNRBoblin);
    CEbin = zeros(1,length(SNREvelin));
    for z = 1:length(SNREvelin)
        CEbin(z) = bawgn(1/SNREvelin(z));
    end  
    CsBin = CBbin - CEbin;
    CsBin(CsBin<0) = 0;
    CsGauss=.5*(log2(1+SNRBoblin)-log2(1+SNREvelin));    
    CsGauss(CsGauss<0)=0;
    
    
    p=semilogy(sigmawstar2,2.^(-CsGauss*n),lineGauss{k});%,sigmawstar2,0.5*(log2(1+SNRBoblin))*ones(1,length(sigmawstar2)));
    
    hold on
    p(1).LineWidth=1.5;
    p(1).DisplayName=['Gaussian, $\sigma_{w_B}^2=$', num2str(-SNRBob),' dB'];
    p(1).Color='k';
    p=semilogy(sigmawstar2,2.^(-CsBin*n),lineBin{k});
    p(1).LineWidth=1.5;
    p(1).DisplayName=['Binary, $\sigma_{w_B}^2=$', num2str(-SNRBob),' dB'];
    k=k+1;
    p(1).Color='k';
end


ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position=13 * [1,1,1,1];
movegui('center')
ax.FontSize=10;
grid on
xlabel("$\sigma_{\omega}^2$ [dB]",'Interpreter','Latex','FontSize',14)
ylabel("$P_{\rm pred}$",'Interpreter','Latex','FontSize',14)
l=legend('show');
l.Interpreter='latex';
l.FontSize=12;
l.Location='northwest';
xlim([sigmawstar2(1) sigmawstar2(end)])
ylim([1e-30 1])


