clear all
close all
SNRBob=db2pow(5);
SNREve=db2pow(0);

sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
CBob=1+Hp0/log(2);

sigma=SNREve; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
CEve=1+Hp0/log(2);

CEve;
R=CBob-0.15*CBob;
n=250; %find nbar such that 1-Pe for eve is like 1e-4. Adjust Rate
biAwgnF(SNRBob,R,n)
log10(1-biAwgnF(SNREve,R,n))

nBar=n;
log2M=R*nBar;
for currn=1:nBar
    Rcurr=log2M/currn;
    Psucc(currn)=max(1-biAwgnF(SNREve,Rcurr,currn),1/(2^log2M));
end
semilogy(1:nBar,Psucc)
grid on