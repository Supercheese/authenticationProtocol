close all 
clear all
r=@(x) standardPulse(x,0);
f1=figure;
subplot(1,2,1)
f=fplot(r,[1e-5 1]);
f.LineWidth=1.5;
xlim([0 1])
ylim([-1.5 1.5])
% xlabel("$t/T_c$",'Interpreter','latex','FontSize',14)
grid on
ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
ax.FontSize=10;
xlabel("$t/T_c$",'Interpreter','latex','FontSize',14)
ylabel("$u_1(t)$",'Interpreter','latex','FontSize',14)

r=@(x) standardPulse(x,2);
subplot(1,2,2)
f=fplot(r,[1e-5 1]);
f.LineWidth=1.5;
xlim([0 1])
ylim([-1.5 1.5])
grid on
ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
ax.FontSize=10;
xlabel("$t/T_c$",'Interpreter','latex','FontSize',14)
ylabel("$u_2(t)$",'Interpreter','latex','FontSize',14)

f1.Units='centimeters';
f1.Position=13 * [1 1 1.7 1];
movegui(f1,'center')