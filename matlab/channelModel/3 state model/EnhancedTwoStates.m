clc
clear all
close all
%% Input Parameters
RouteLength=100; %in meters
Lframe=5; % in meters
Lcorr=10; %correlation length in meters
freq=2200e6;         % frequency in MHz      
lambdac=300e6/freq;  % wavelength in m
sampling_factor=20;         
sample_space=lambdac/sampling_factor;    % sampling spacing
InterpRate=round(Lframe/sample_space);
state_transitions=round(RouteLength/Lframe);
Nslowsamples=RouteLength/Lcorr; % No of slow samples
NFFT=1024;
%% State1 Parameters
Nsamples=RouteLength/sample_space;
directmean_mean_state1=-0.4391; % gaussian distributed direct signal mean's mean
directmean_var_state1=0.2745; % gaussian distributed direct signal mean's standard deviation
MP_mean_state1=-15.1989;  %
MP_var_state1=1.9900; %
%% State2 Parameters
directmean_mean_state2=-9.9041; % gaussian distributed direct signal mean's mean
directmean_var_state2=2.1419; % gaussian distributed direct signal mean's standard deviation
MP_mean_state2=-13.9104;  %Multipath mean
MP_var_state2=1.6586; % Multipath variance
%%  
    directmean_state1=directmean_mean_state1+(directmean_var_state1*randn(1,state_transitions));    
   
    directstd_mean_state1=-0.6783.*directmean_state1.^2-0.8502*directmean_state1+0.3338;
   
    directstd_var_state1=-0.0101.*directmean_state1.^2-0.091*directmean_state1+0.0739;
    
    directstd_state1=directstd_mean_state1+(directstd_var_state1.*randn(1,state_transitions));
    
    directmean_state2=directmean_mean_state2+(directmean_var_state2.*randn(1,state_transitions));
    
    directstd_mean_state2=-0.0814.*directmean_state2.^2-1.4955.*directmean_state2-3.6693;
     
    directstd_var_state2=0.0191.*directmean_state2.^2+0.2555.*directmean_state2+1.2891;

    directstd_state2=directstd_mean_state2+(directstd_var_state2.*randn(1,state_transitions));
%% Generation of rayleigh samples for state1
    MP_state1=MP_mean_state1+MP_var_state1*randn(1,state_transitions);
    sigma1=sqrt((10.^(MP_state1/10))/2);
%  sigma1=10.*sigma1;
 %% Generation of rayleigh samples for state2
    MP_state2=MP_mean_state2+MP_var_state2*randn(1,state_transitions);
    sigma2=sqrt((10.^(MP_state2/10))/2);
%  sigma2=10.*sigma2;
%% =============== Butterworth filter parameters =========================
    Wp=0.1;            
    Ws=0.3;
    Rp=3; %dB
    Rs=40; %dB
 %%  
    channel_state = zeros(1,state_transitions); % 2-state Markov chain (output vector).
    channel_state(1) = randi([1 2],1,1);
for i = 2:1:state_transitions
    if(channel_state(1,i-1) == 1) 
         channel_state(1,i) = 2; 
    else
         channel_state(1,i) = 1;
    end
end
    InterpStateSeries=[];
for ii=1:length(channel_state)
    if (channel_state(ii)==1), 
        InterpStateSeries=[InterpStateSeries; channel_state(ii);ones(InterpRate-1,1)];
    elseif (channel_state(ii)==2)
        InterpStateSeries=[InterpStateSeries; channel_state(ii);2*ones(InterpRate-1,1)];  
    end
end
    subplot(211);   
    plot(InterpStateSeries,'r');
    distance_axis=[0:length(InterpStateSeries)-1]*sample_space;
    aa=axis;
    axis([aa(1) aa(2)  0 4])
    looseries_state1=[];
    looseries_state2=[];
    time_series=[];
    looseries_state1=[];
    looseries_state2=[];
for i=1:state_transitions   
    direct_gauss_state1=directmean_state1(i)+(directstd_state1(i)*randn(1,InterpRate));
    direct_gauss_state2=directmean_state2(i)+(directstd_state2(i)*randn(1,InterpRate));
    direct_lognormal_state1=10.^(direct_gauss_state1./10);
    direct_lognormal_state2=10.^(direct_gauss_state2./10)/max(10.^(direct_gauss_state2./10));
    I=randn(1,InterpRate);
    Q=randn(1,InterpRate);
    rayleigh=I+1i*Q;
    [ray_filt]=uu_satcomsys_filtersignal(rayleigh,Wp,Ws,Rp,Rs);
    ray_filt_state1=sigma1(i)*ray_filt;
    ray_filt_state2=sigma2(i)*ray_filt;
    if(channel_state(i)==1)
            looseries_state1_segment=direct_lognormal_state1+ray_filt_state1;
            looseries_state1=[looseries_state1 looseries_state1_segment];
            time_series=[time_series looseries_state1_segment];
    else
            looseries_state2_segment=direct_lognormal_state2+ray_filt_state2;
            looseries_state2=[looseries_state2 looseries_state2_segment];
            time_series=[time_series looseries_state2_segment];
    end
end
subplot(212);
plot(20*log10(abs(time_series))-max(20*log10(abs(time_series))))
grid on;
[a,b]=uu_satcomsys_cdf(abs(time_series));
figure,plot(a,b);
axis([0 2 0 1])        
figure,plot(directstd_state1,directmean_state1,'*',directstd_state2,directmean_state2,'ro');  
%  state duration statistics
sigma_duration=1.11;
alpha_duration=log(1.73);
sigmadB=20*log10(sigma_duration);
alphadB=20*log10(alpha_duration);
gauss_duration=sigmadB*randn(1,100000)+alphadB;
lognormal_duration=10.^(gauss_duration/10);
figure,plot(lognormal_duration);
[a,b]=uu_satcomsys_cdf(lognormal_duration);
figure,plot(a,b)
