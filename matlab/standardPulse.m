function [ tot ] = standardPulse(x,mask)
%Standard pulse Galielo. Chip in interval [0,1]
alpha= sqrt(10/11);
beta= sqrt(1/11);
p=alpha*sign(sin(2*pi.*x));
sub=beta*sign(sin(2*pi*6.*x));
tot=p+sub;

if mask==1 %one horn
    tot(x<4/12)=0;
    tot(x>8/12)=0;
elseif mask==2 % two horns
    tot(x>2/12 & x<10/12)=0;
    tot(x>2/12 & x<10/12)=0;
elseif mask==3
    tot(:)=1;
% tot=tot./sqrt(tot.*tot);
end

