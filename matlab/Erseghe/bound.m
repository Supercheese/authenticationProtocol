n=2048;
k=1024;
R=k/n;
sigma=db2pow(15);
alpha=sqrt(0.25*sigma);

%% lambda
lambda=sqrt(1-2^(-2*R)*exp(log(n)/n));

%% G G' G''
ni=n/2;
syms s;
G=sqrt(1+s^2) - 1 - (1/(2*ni))*...
    log(sqrt(1+s^2)) - ...
    (1-1/ni)*...
    log((1+sqrt(1+s^2))/(2));
Gprimo=diff(G);
Gsecondo=diff(Gprimo);

%% w0(a)

syms a;
w1=alpha*a+sqrt(1+(alpha*a)^2);
w0=sqrt((1-a^2)/(a^2)*(1+a*alpha*w1))...
    *(2*alpha*(1-a^2)*w1-a)...
    *(2*a-2*alpha*(1-a^2)*w1);

%% u0(a)
u1=log(1+(a*alpha)^2+alpha*a*sqrt(1+(alpha*a)^2))+...
    3*log(1-a^2)+log(2*pi);
u0=.5*log(1-a^2)-2*alpha^2 ...
    +(alpha*a)^2+alpha*a*sqrt(1+(alpha*a)^2) ...
    +log(alpha*a+sqrt(1+(alpha*a)^2));

%% slam
slam=subs(finverse(Gprimo),s,-a);
syms lam
vn=-.5*(subs(G,s,slam)+a*slam)+...
    1/(2*n)*log(pi*n*(slam)^2*subs(Gsecondo,s,slam));

eqn=subs(vn,a,lam)==R*log(2);
vpasolve(eqn,lam)
% eqn=Gprimo+lambda==0;
% slam=vpasolve(eqn,s);

%% Pe
lnPe=vpa(subs(u0,a,lambda));