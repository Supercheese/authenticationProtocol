clear all
% close all


nSamp=100;% samples per chip pulse (continuos time approx)
load rangingCodes.mat
ss1=c4;
Ns=length(c4);
tt=nSamp*Ns; %total taps for one symbol

ss=(randi(2,1,4000)-1);
ss(ss==0)=-1;
g=buildPulse(nSamp,0,0,ss,"standard");
% g=g/sqrt(g*g');

% g=g/sqrt(g*g');% unit energy on T_s

% plot(g)
% 
% nData=1e1;
% d=randi(2,1,nData)-1;
% x=kron(d,g);
% 
% [pxx w]=pwelch(x);
% plot(w,pxx)

R1=1.023*1e6; %MHz
R2=6.138*1e6;

T2=1/R2;
T1=1/R1; % chip period
fs=(T1/nSamp)^-1;

[pxx, f]=periodogram(g,[],length(g),fs);
[pxx, f]=pwelch(g,[],[],[],fs);

plot(f/1e6,10*log10(pxx))
grid on
xlabel("MHz")
% xlim([0,10])
hold on

ss=(randi(2,1,4000)-1);
ss(ss==0)=-1;
g=buildPulse(nSamp,0,0,ss,"hornStandard");
% g=g/sqrt(g*g');
[pxx1, f]=periodogram(g,[],length(g),fs);
[pxx1, f]=pwelch(g,[],[],[],fs);
% figure
plot(f/1e6,10*log10(pxx1))
xlabel("MHz")
% xlim([0,10])
ylim([-110,-60])
grid on