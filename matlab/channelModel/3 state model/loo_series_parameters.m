                                % loo series parameters
%% LOS State (State1) parameters
mean_state1=-0.4;                                    % Mean of lognormal samples(dB)
sigma_state1=1.5;                                    % standard deviation of lognormal samples(dB)
multipath_power_state1=-13.2;                        % avergae Multipath power(dB)

%% Moderate Shadowing State (State2) parameters
mean_state2=-8.2;                                    % Mean of lognormal samples(dB) 
sigma_state2=3.9;                                    % standard deviation of lognormal samples (dB)
multipath_power_state2=-12.7;                        % avergae Multipath power(dB)

%% Deep Shadowing State (State3) parameters
mean_state3=-17;                                     % Mean of lognormal samples(dB) 
sigma_state3=3.14;                                   % standard deviation of lognormal samples(dB)
multipath_power_state3=-10.0;                        % avergae Multipath power (dB)