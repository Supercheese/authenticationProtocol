clear all
load equivTapsRectDelay
pulsetype='rect';
plots=0;

Nc=4092; % spreading seq length

% Noise
sigmawstarChip=100; % w*
sigmawstar=db2pow(-6); % symbol noise w*
SNRbob=16; %dB after despreader
sigmawB=db2pow(-SNRbob); % symbol noise w_B given unit energy pulse
sigmaTot=sigmawB+sigmawstar; % total symbol noise

h=comm.LDPCEncoder;
hdec=comm.LDPCDecoder('IterationTerminationCondition','Parity check satisfied');
pskMod=comm.PSKModulator('PhaseOffset',0,'BitInput',1,'ModulationOrder',2);
pskDemod=comm.PSKDemodulator('PhaseOffset',0,'BitOutput',1,...
    'DecisionMethod','Log-likelihood ratio', 'Variance',sigmaTot...
    ,'ModulationOrder',2);
H=h.ParityCheckMatrix;
n=length(H(1,:));
k=n-length(H(:,1));

authBit=round(k*(6/8)); % number of authenticated bits
mask1=logical([ones(k,1);zeros(n-k,1)]); % mask for tx bit
mask2=logical([zeros(k,1); ones(authBit,1);zeros(n-k-authBit,1)]); %mask for auth bit
mask3=logical([zeros(k+authBit,1); ones(n-k-authBit,1)]); % mask for deleted bits

%% simulation

delay=1:10:101;

Nit=1e1;
nerr=zeros(1,length(delay));
tic
for it=1:Nit
    if mod(it,1e2)==0
        it
    end
    % secret message
    u=double(randn(k,1)>0);
    
    % encode
    c=h(u);
    
    %set up llrs
    llr=zeros(n,1);
    llr(mask2)=inf;
    llr(c>0)=-inf; %set authenticated bits
    llr(mask3)=0; %lost bits
    
    % send only k bits. The rest is auth channel
    cSent=c(1:k);
    
    % modulate
    ak=pskMod(cSent);
    ns=length(ak); % number of txmitted symbols
    akHat=zeros(1,ns);
    
    %noise
    wB=sqrt(sigmaTot/2)*(randn(1,ns)+1j*randn(1,ns));
    
    % channel
    for d=1:length(delay)
        
        for z=1:ns-1
            akHat(z)=alpha(delay(d))*ak(z)+beta(delay(d))*ak(z+1);
        end
        akHat(ns)=alpha(delay(d))*ak(ns); %last symbol
        
        %noise
        rB=akHat+wB;
        
        %demodulate
        llr(mask1)=pskDemod(rB.'); % llr
        
        %decode
        uHat=hdec(llr);
        
        nerr(d)=nerr(d)+sum(u~=uHat);
    end
end
toc
accuracy=10/(Nit*k);
Pbit=nerr/(k*Nit);
delay=delay/100;
if plots
    figure
    semilogy(delay,Pbit)
    grid on
    xlabel('$\epsilon/T_c$','Interpreter','latex')
    ylabel('$P_{bit}$','Interpreter','latex')
    title([pulsetype ' $\sigma_w^*=$' num2str(pow2db(sigmawstar)) ' [dB]' ],...
        'Interpreter', 'Latex')
    xlim([0,1]);
end