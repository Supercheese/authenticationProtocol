SNRval = 0:0.2:10;
SNR=db2pow(SNRval);


for k=1:length(SNR)
    n = 500;
    R=0.4;
    integrand= @(x,l) exp(-1/(2*SNR(k))*(x-SNR(k)).^2).*  ...
        (-log(1+exp(-2.*x))).^l;
    
%     fplot(@(x) integrand(x,1),[-100,100])
    
    Hp0=1/sqrt(2*pi*SNR(k))*integral(@(x) integrand(x,1),-10,10,'AbsTol',1e-12);
    CB=1+Hp0/log(2);
    
    
    Hs0=1/sqrt(2*pi*SNR(k))*integral(@(x) integrand(x,2),-10,10,'AbsTol',1e-12);
    V=Hs0-Hp0^2;
    
    PeB(k) = qfunc(sqrt(n/V)*( (CB-R)/log2(exp(1)) + log(n)/(2*n)));
    
    % R=log2M/n;
    CG=.5*log2(1+SNR(k));
    VG=SNR(k)*(2+SNR(k))/(2*(1+SNR(k))^2);
    PeG(k) = qfunc(sqrt(n/VG)*( (CG-R)/log2(exp(1)) + log(n)/(2*n)));
end

figure
semilogy(SNRval,PeG,'DisplayName','Gaussian')
hold on
semilogy(SNRval,PeB,'DisplayName','Binary')
legend('show')
grid on