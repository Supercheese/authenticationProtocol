close all
clear all
% Tc=1;
alpha = sqrt(10/11);
beta = sqrt(1/11);
Tc=1/1.023e6;

ZF=12/Tc; % first zero of sinc(fTc/12)
f=ZF/100:ZF/100:5*ZF;
f=[-flip(f),0,f];

bAbs2=((Tc/12)*sinc(f*Tc/12)).^2;

compl = @(f,i) exp(-2*1j*pi*f*i*Tc/12);
% f=10;
H1Abs2=bAbs2.*abs( (alpha+beta)*compl(f,4)+...
    (alpha-beta)*compl(f,5)+...
    (beta-alpha)*compl(f,6)+...
    (-alpha-beta)*compl(f,7) ).^2;

H2Abs2=bAbs2.*abs( (alpha+beta)*compl(f,0)+...
    (alpha-beta)*compl(f,1)+...
    (beta-alpha)*compl(f,10)+...
    (-alpha-beta)*compl(f,11) ).^2;

HSAbs2=bAbs2.*abs( (alpha+beta)*compl(f,0)+...
    (alpha-beta)*compl(f,1)+...
    (alpha+beta)*compl(f,2)+...
    (alpha-beta)*compl(f,3)+...
    (alpha+beta)*compl(f,4)+...
    (alpha-beta)*compl(f,5)+...
    (beta-alpha)*compl(f,6)+...
    (-alpha-beta)*compl(f,7)+...
    (beta-alpha)*compl(f,8)+...
    (-alpha-beta)*compl(f,9)+...
    (beta-alpha)*compl(f,10)+...
    (-alpha-beta)*compl(f,11) ).^2;

f1=figure;
H1Abs2(H1Abs2==0)=1e-10*Tc;
% p=plot(f/1e6,10*log10(H1Abs2/Tc),'-dr');
% p.MarkerIndices=1:3:length(f);
% xlim([-10,10])
% ylim([-100,-50])
% p.LineWidth=1.5;
% p.DisplayName="$x_1(t)$";

grid on
hold on


% plot(10*log10(abs(fft(chipPulse(10000,"1Horn"))).^2))

HSAbs2(HSAbs2==0)=1e-10*Tc;
p=plot(f/1e6,10*log10(HSAbs2/Tc),'-k');
xlim([-10,10])
ylim([-100,-60])
p.LineWidth=1.5;



p.DisplayName="$u_1(t)$";
grid on

ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f1.Units='centimeters';
f1.Position=15*[1 1 1.3 0.5];
movegui(f1,'center')
ac=gca;
ac.FontSize=10;
xlabel("$f$ [MHz]",'Interpreter','latex','FontSize',14)
ylabel("$\mathcal{P}(f) \ [V^2/Hz]$",'Interpreter','latex','FontSize',14)

H2Abs2(H2Abs2==0)=1e-10*Tc;
p=plot(f/1e6,10*log10(H2Abs2/Tc),'--k');
p.MarkerIndices=1:3:length(f);
xlim([-10,10])
ylim([-100,-60])
p.LineWidth=1.5;
p.DisplayName="$u_2(t)$";
grid on
l=legend("show");
l.Interpreter='latex';
l.FontSize=12;

