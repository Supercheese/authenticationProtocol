clear all
close all
SNR=db2pow(1);

lineBin={'-.','--',':'};
lineGauss={'-.d','--d',':d'};


f=figure;


integrand= @(x,l) exp(-1/(2*SNR)*(x-SNR).^2).*  ...
    (-log(1+exp(-2.*x))).^l;
Hp0=1/sqrt(2*pi*SNR)*integral(@(x) integrand(x,1),-10,10);
CB=1+Hp0/log(2);
Hs0=1/sqrt(2*pi*SNR)*integral(@(x) integrand(x,2),-10,10);
VB=Hs0-Hp0^2;


CG=.5*log2(1+SNR);
VG=SNR*(2+SNR)/(2*(1+SNR)^2);


nBar=500;
k=1;
markerIndices = 1:10:500;
for R = [0.45 0.5]
    for currn=100:nBar
        PsuccG(currn)=qfunc(sqrt(currn/VG)*( (CG-R)/log2(exp(1)) + log(currn)/(2*currn)));
        Psuccb(currn)=qfunc(sqrt(currn/VB)*( (CB-R)/log2(exp(1)) + log(currn)/(2*currn)));
    end
    p = semilogy(1:nBar,PsuccG,lineGauss{k},'MarkerIndices',markerIndices,'Color','k');
    p.DisplayName = [ 'Gaussian ', '$R_x=' num2str(R) '$'];
    p.LineWidth = 1.5;
    hold on
    p = semilogy(1:nBar,Psuccb,lineBin{k},'Color','k');
    k = k + 1;
    p.DisplayName = ['Binary ','$R_x=' num2str(R) '$'];
    p.LineWidth = 1.5;
    
    
end


% %% Binary
% 
% sigma=SNRBob; %just change names
% nBar=250;
% k=1;
% for R = [0.01 0.1 0.5]
%     for currn=1:nBar
%         PsuccG(currn)=biAwgnF(sigma,R,currn);
%     end
%     p = semilogy(1:nBar,PsuccG,lineBin{k},'Color','k');
%     k = k + 1;
%     p.DisplayName = ['Binary ','$R_x=' num2str(R) '$'];
%     p.LineWidth = 1.5;
%     hold on
% end

% ylim([1e-10,1])
% xlim([1,100])
ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position= 13 * [1 1 1 1];
movegui('center')
ax.FontSize=10;
grid on
xlabel("$\bar{n}$",'Interpreter','latex','FontSize',14);
ylabel("$q$",'Interpreter','latex','FontSize',14);
l = legend("show");
l.Interpreter = 'latex';
l.FontSize=12;
grid on

% addpath("../reorderLegend")
% reorderLegend([6,3,5,2,4,1])