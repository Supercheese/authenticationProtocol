clear all
close all


nSamp=100; %samples per chip
R1=1.023*1e6; %MHz
R2=6.138*1e6;
T2=1/R2;
T1=1/R1; % chip period
fs=(T1/nSamp)^-1;

load rangingCodes.mat %c1 c2 c4

% Standard chip pulse
g=chipPulse(nSamp,"standard");
Es=g*g';

s=kron(c1,g);
psdSamp=nSamp*1000;
sPsd=s(1:end);
[pxs, f]=periodogram(sPsd,[],length(s),fs);
[pxs, f]=pwelch(sPsd,[],[],[],fs);

p=plot(f/1e6,pow2db(pxs));
p.DisplayName="$p(t)$";
ylim([-100,-40])
xlim([0,10])
hold on
grid on

%% noise
wStar=randn(1,length(s)); % genera rumore lungo quanto simbolo
wStar=wStar-(wStar*s')*s/(s*s'); %project. (s*s') is because s non orthonormal
sigmaWStar2=db2pow(-6); % noise power
wStar=sqrt(sigmaWStar2)*wStar;

%% 1Horn
g=chipPulse(nSamp,"1Horn");
g=sqrt(Es/(g*g'))*g; % same energy as before

sx=kron(c2,g);
x1=sx;%+wStar;

x1Psd=x1(1:end);
[px1, f]=periodogram(x1Psd,[],length(s),fs);
[px1, f]=pwelch(x1Psd,[],[],[],fs);
p=plot(f/1e6,pow2db(px1));
p.DisplayName="$x_1(t)$";


%% 2Horn
g=chipPulse(nSamp,"2Horn");
g=sqrt(Es/(g*g'))*g; % same energy as before

sx=kron(c2,g);
x2=sx;%+wStar;

x2Psd=x2(1:end);
[px2, f]=periodogram(x2Psd,[],length(s),fs);
[px2, f]=pwelch(x2Psd,[],[],[],fs);
p=plot(f/1e6,pow2db(px2));
p.DisplayName="$x_2(t)$";

l=legend("show");
l.Interpreter='latex';
l.FontSize=13;
xlabel("MHz",'Interpreter','latex','FontSize',14)
ylabel("$\mathcal{P}(f)$ dB", 'Interpreter', 'latex')

%% plot noise
% figure;
% [pw, f]=periodogram(wStar,[],length(wStar),fs);
% plot(f,pow2db(pw))
% grid on
% xlabel("MHz")
% ylabel("$\mathcal{P}(f)$ dB",'Interpreter', 'latex')
% title(strcat("PSD of $w^*(t)$ with  $\sigma_{w^*}^2 = $",...
%     num2str(pow2db(sigmaWStar2))),'Interpreter','latex')