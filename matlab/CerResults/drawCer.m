clear all
close all
 %% rect
 load CerRectFullRate
 rect=[Cer(4:end) 1];
 rectdel=[delay(4:end) 1];
 delay1=[0:.1:delay(4) delay(4)];
 delay1tot=[-flip(delay1) delay1];
 recttot=[flip(rect) rect];
 rectdeltot=[-flip(rectdel) rectdel];
 figure
 p=semilogy(rectdeltot,recttot,delay1tot,Cer(4)*ones(size(delay1tot)));
 p(1).LineWidth=1.5;
 p(2).LineWidth=1.5;
 p(1).DisplayName='Simulations';
 p(2).DisplayName='1e-3 threshold';
 ylabel('CER','FontSize',14,'Interpreter','latex')
 xlabel('$\epsilon/T_c$','FontSize',14,'Interpreter','latex')
 grid on
 legend('show')
 title('Rect')
 %% 2 horn
 clear all
 
 load CerTwoHornFullRate3
 twohorn=Cer(2:end);
 twohorndel=delay(2:end);
 load CerTwoHornFullRate2
 twohorn=[twohorn Cer 1];
 twohorndel=[twohorndel delay 1];
 delt=[-twohorndel(1) twohorndel(1)];
 thtot=[flip(twohorn) twohorn];
 thdtot=[-flip(twohorndel) twohorndel];
 figure
 p=semilogy(thdtot,thtot,delt,twohorn(1)*ones(size(delt)));
 
 p(1).LineWidth=1.5;
 p(2).LineWidth=1.5;
 p(1).DisplayName='Simulations';
 p(2).DisplayName='1e-3 threshold';
 ylabel('CER','FontSize',14,'Interpreter','latex')
 xlabel('$\epsilon/T_c$','FontSize',14,'Interpreter','latex')
 grid on
 legend('show')
 title('TwoHorn')