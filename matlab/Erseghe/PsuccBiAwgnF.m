function [ Psucc ] = PsuccBiAwgnF( SNRBob,SNREve )

sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;

nBar=100; % set nbar such that Psucc for Eve is like 1e-6
log10((1-biAwgnF(SNREve,C,nBar)));
log2M=C*nBar;

z=1;
for n=1:nBar
    R=log2M/n;
    oneMPe(z)=(1-biAwgnF(SNREve,R,n));
    z=z+1;
 end
 Psucc=(1/2^log2M)*(1-oneMPe) + oneMPe;

end

