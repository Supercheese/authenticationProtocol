clear all
close all
SNRBob=db2pow(5);
SNREve=db2pow(0);
Pe=1e-3; % target Pe for Bob
nBar=250;

%compute V and C
Gamma=SNRBob;
C=.5*log2(1+Gamma);
V=Gamma*(2+Gamma)/(2*(1+Gamma)^2);
R1=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2gamma=R1*nBar;

sigma=SNREve; %just change names

for currn=1:nBar
    Rcurr=log2gamma/currn;
    Peve=normalApproxF(log2gamma,currn,sigma); %compute Eve's Pe
    Psucc(currn)=max(1-Peve,1/(2^log2gamma));
end
p=semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB']);
p.LineWidth=1.5;
p.Color='k';
f=figure(1);
grid on
hold on

%%

SNRBob=db2pow(5);
SNREve=db2pow(2);
Pe=1e-3; % target Pe for Bob
nBar=250;

%compute V and C
Gamma=SNRBob;
C=.5*log2(1+Gamma);
V=Gamma*(2+Gamma)/(2*(1+Gamma)^2);
R1=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2gamma=R1*nBar;

sigma=SNREve; %just change names

for currn=1:nBar
    Rcurr=log2gamma/currn;
    Peve=normalApproxF(log2gamma,currn,sigma); %compute Eve's Pe
    Psucc(currn)=max(1-Peve,1/(2^log2gamma));
end
p=semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB']);
p.LineWidth=1.5;
p.LineStyle='-.';
p.Color='k'
grid on
hold on

SNRBob=db2pow(5);
SNREve=db2pow(4);
Pe=1e-3; % target Pe for Bob
nBar=250;

%compute V and C
Gamma=SNRBob;
C=.5*log2(1+Gamma);
V=Gamma*(2+Gamma)/(2*(1+Gamma)^2);
R1=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
log2gamma=R1*nBar;

sigma=SNREve; %just change names

for currn=1:nBar
    Rcurr=log2gamma/currn;
    Peve=normalApproxF(log2gamma,currn,sigma); %compute Eve's Pe
    Psucc(currn)=max(1-Peve,1/(2^log2gamma));
end
p=semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB']);
p.LineWidth=1.5;
p.LineStyle='--';
p.Color='k';
grid on
hold on

% SNRBob=db2pow(5);
% SNREve=db2pow(10);
% Pe=1e-3; % target Pe for Bob
% nBar=250;
% 
% %compute V and C
% Gamma=SNRBob;
% C=.5*log2(1+Gamma);
% V=Gamma*(2+Gamma)/(2*(1+Gamma)^2);
% R1=C- sqrt(V/nBar)*log2(exp(1))*qfuncinv(Pe)+log2(nBar)/(2*nBar);
% log2gamma=R1*nBar;
% 
% sigma=SNREve; %just change names
% 
% for currn=1:nBar
%     Rcurr=log2gamma/currn;
%     Peve=normalApproxF(log2gamma,currn,sigma); %compute Eve's Pe
%     Psucc(currn)=max(1-Peve,1/(2^log2gamma));
% end
% p=semilogy(1:nBar,Psucc,'DisplayName',['$\Gamma_E=$',num2str(pow2db(SNREve)),' dB']);
% p.LineWidth=1.5;
% p.LineStyle=':';
% p.Color='k';

grid on
hold on
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui(f,'center')
ax=gca;
ax.FontSize=10;

ylim([1e-15 2e0])
xlim([60,250])
l=legend('show');
l.Interpreter='latex';

xlabel('$n$','Interpreter','latex','FontSize',14)
ylabel('$P_{succ}$','Interpreter','latex','FontSize',14)
l.FontSize=12;