function [ out ] = buildPulse( nSamp,sxTaps,dxTaps,ss, pulseMode )
% nSamp for each chip; ss is binary spreading sequence.
% out is spreading symbol pulse

if pulseMode=="horn"
    pulse=[ones(1,sxTaps) zeros(1,nSamp-sxTaps-dxTaps) ones(1,dxTaps)];
    out=kron(ss,pulse);
elseif pulseMode=="standard"
    x=(1:nSamp)/nSamp;
    pulse=standardPulse(x,0);
    out=kron(ss,pulse);
elseif pulseMode=="1hornStandard"
    x=(1:nSamp)/nSamp;
    pulse=standardPulse(x,1);
    out=kron(ss,pulse);
elseif pulseMode=="2hornStandard"
    x=(1:nSamp)/nSamp;
    pulse=standardPulse(x,2);
    out=kron(ss,pulse);
end

end

