function [ chip ] = chipPulse(nSamp,pulseType)

x=1/nSamp:1/nSamp:1;
alpha= sqrt(10/11);
beta= sqrt(1/11);
sub1=alpha*sign(sin(2*pi.*x));
sub2=beta*sign(sin(2*pi*6.*x));
chip=sub1+sub2;

if pulseType=="standard"
elseif pulseType=="1Horn"
    chip(x<4/12)=0;
    chip(x>8/12)=0;
elseif pulseType=="2Horn"
    chip(x>2/12 & x<10/12)=0;
end

end