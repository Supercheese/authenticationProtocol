function [ Pe ] = biAwgnF( SNR,R,n)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
sigma=SNR; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

% % plot integrand
% f=figure;
% fplot(@(x) integrand(x,2),[-10,10])


Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);
close(f)
Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;

Pe=qfunc(sqrt(n/V)*( (C-R)/log2(exp(1)) + log(n)/(2*n)));

end

