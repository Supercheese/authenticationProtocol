close all
clear all

SNRBob=db2pow(5);
SNREve=db2pow(-6);

%% given SNR --> C and transmission rate compute Pe 
sigma=SNRBob; %just change names
integrand= @(x,l) exp(-1/(2*sigma)*(x-sigma).^2).*...
    (-log(1+exp(-2.*x))).^l;

% % plot integrand
% figure
% fplot(@(x) integrand(x,2),[-10,10])

Hp0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,1),-10,10);
C=1+Hp0/log(2);

Hs0=1/sqrt(2*pi*sigma)*integral(@(x) integrand(x,2),-10,10);
V=Hs0-Hp0^2;



% %% Capacit� alternativa
% close all
% 
% integrand1= @(x) exp(-(sigma/2)*(x-1).^2).*...
%     log(1+exp(-2*sigma.*x));
% % 
% % figure
% % fplot(integrand1,[-10,10])
% 
% C1=1-sqrt(sigma/(2*pi))*integral(integrand1,-10,10);

% %% Using the function
% nBar=100; % set nbar such that Psucc for Eve is like 1e-6
% log10((1-biAwgnF(SNREve,C,nBar)));
% log2M=C*nBar;
% 
% z=1;
% for n=1:nBar
%     R=log2M/n;
%     oneMPe(z)=(1-biAwgnF(SNREve,R,n));
%     z=z+1;
%  end
%  Psucc=(1/2^log2M)*(1-oneMPe) + oneMPe;
%  figure
%  semilogy(1:z-1,Psucc)
%  grid on
%
% CB=0.4859; % obtained with Gamma=0. C=0.4859 

R=C-1e0;
n=250;
Pe=qfunc(sqrt(n/V)*( (C-R)/log2(exp(1)) + log(n)/(2*n)));
log10((1-biAwgnF(SNREve,R,n)))

Psucc=PsuccBiAwgnF(db2pow(0),db2pow(-6));
figure
p=semilogy(1:length(Psucc),Psucc,'-d','MarkerIndices',1:5:length(Psucc));
p.LineWidth=1.5;
p.DisplayName="$\Gamma_B=0$ dB, $\Gamma_E=-6$ dB";
grid on
hold on

Psucc=PsuccBiAwgnF(db2pow(0),db2pow(0));
p=semilogy(1:length(Psucc),Psucc);
p.LineWidth=1.5;
p.DisplayName="$\Gamma_B=0$ dB, $\Gamma_E=0$ dB";

Psucc=PsuccBiAwgnF(db2pow(0),db2pow(5));
p=semilogy(1:length(Psucc),Psucc);
p.LineWidth=1.5;
p.DisplayName="$\Gamma_B=0$ dB, $\Gamma_E=5$ dB";

l=legend("show");
l.Interpreter='latex';
l.FontSize=12;
ac=gca;
ac.FontSize=12;

xlabel("$n$",'Interpreter','latex','FontSize',14)
ylabel("$P_{succ}$",'Interpreter','latex','FontSize',14)