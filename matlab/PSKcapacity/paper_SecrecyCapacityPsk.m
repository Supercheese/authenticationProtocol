%% PSK channel, secrecy capacity as function of sigmawstar
%% Stand alone, just insert SNRs

clear all
close all
warning('error','MATLAB:integral2:nonFiniteResult');

n =250;
sigmawB=-5;
sigmawBlin=db2pow(sigmawB);
sigmawBhalf=sigmawBlin/2;
sigmawstar=sigmawB-3:10;
sigmawstarlin=db2pow(sigmawstar);
sigmawstarhalf=sigmawstarlin/2;

f=figure;
hold on
for M=[2 4 8 ]
    fun=@(x,y) integrand(x,y,sigmawBhalf,M);
    CB=integral2(fun,-4,4,-4,4)-log2(2*pi*exp(1)*sigmawBhalf);
%       fsurf(fun,[-5 5 -5 5])
    
    xyInterval=50*[-1,1,-1,1];
    CE=zeros(size(sigmawstar));
    % CE(1)=CB;
    
    IO=[1 -1 1 -1];
    for z=1:length(sigmawstar)
        fun=@(x,y) integrand(x,y,sigmawstarhalf(z),M);
        
        II=[-40 40 -40 40];
        success=0;
        while ~success
            try
                CE(z)=integral2(fun,II(1),II(2),II(3),II(4))-log2(2*pi*exp(1)*sigmawstarhalf(z));
                success=1;
            catch w
                II=II+IO;
            end
        end
%             fsurf(fun,xyInterval)
    end
    Cs=CB-CE;
    Cs(Cs<0)=0;
    
    p=plot(sigmawstar,log10(2.^(-Cs*n)));
    p.DisplayName=['M = ' num2str(M)];
    p.LineWidth=1.5;
%     p.Color='k';
%     p1=plot(sigmawstar,CB*ones(size(sigmawstar)));
%     p1.Color=p.Color;
%     p1.LineStyle='--';
%     p1.LineWidth=1;
%     p1.DisplayName='$C_B$';
end
% CsComplex=log2(1+db2pow(16))-log2(1+db2pow(-sigmawstar));
% plot(sigmawstar,CsComplex,'k')
grid on
l=legend('show');
l.Interpreter='latex';
l.Location='northeast';
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui(f,'center')
ax=gca;
ax.FontSize=10;
% xlim([sigmawB-3 20]);
ylim([-20,0])
xlabel('$\sigma^2_{w^*}$ [dB]','Interpreter','latex','FontSize',14)
ylabel('$P_{succ}$','Interpreter','latex','FontSize',14)
l.FontSize=12;

p=get(gca,'Children');
p(2).LineStyle='--';
p(3).Marker='d';


warning('on','MATLAB:integral2:nonFiniteResult');