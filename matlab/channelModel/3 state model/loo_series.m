function [loo_samples] =loo_series(M,S,MP)
loo_series_parameters;
simu_parameters;
sigma=sqrt(0.5*10^(MP/10));         % conversion of average Multipath power to standard deviation of rayleigh samples 
gauss= S*(randn(Nslowsamples,1))+M; % Uncorrelated Gaussian samples of direct component
%% Interpolation of lognormal samples 
lognormal_samples=10.^(gauss/20);   % Uncorrelated lognormal samples of direct component
distance_axis_lcorr=(0:Nslowsamples-1)*Lcorr;         % distance axis with correlation length spacing
distance_axis=(0:Nsamples-1)*sample_spacing;   % distance axis with sample spacing
lognormal_Interp=interp1(distance_axis_lcorr,lognormal_samples,distance_axis,'spline');  % Interpolation of lognormal samples 
 
%% Doppler shift of Direct signal 
phi=kc.*(0:Nsamples-1)*sample_space*cosd(AoA);     
phase_ini=2*pi*rand;                                        
phi=phi+phase_ini;                                          
%% overall complex lognormal samples 
lognormal_complex=lognormal_Interp.*exp(1i*phi);
%% Generate fast variations 
ray_filt = raysim(Nsamples,max_doppler_shift,fs);
ray_filt=sigma.*ray_filt;
%% Loo samples
loo_samples=ray_filt+lognormal_complex;
end





