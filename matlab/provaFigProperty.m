close all
clear all
f=figure;
x=0:100;
y=sin(20^-1*pi*x);
p=plot(x,y);

ax=gca;
ax.PlotBoxAspectRatio=[1 1 1];
f.Units='centimeters';
f.Position=[10 10 10 10];
movegui(f,'center')
ax.FontSize=15;